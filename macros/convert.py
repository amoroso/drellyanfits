import pandas as pd
import numpy as np
import time, copy
import matplotlib.pyplot as plt

# summary of the meaning of the various columns in the NNLOJET data files
#tot_scale01[4] muf = mln mur = mln
#tot_scale02[6] muf = mln mur = 2.0 * mln
#tot_scale03[8] muf = 2.0 * mln mur = mln
#tot_scale04[10] muf = 2.0 mln mur = 2.0 mln
#tot_scale05[12] muf = mln mur = 0.5 * mln
#tot_scale06[14] muf = 0.5 * mln mur = mln
#tot_scale07[16] muf = 0.5 mln mur = 0.5 mln


def readBinCC(fileN): # input file name(string), return pandas df with the file content
    # read the NNLOJET data format file as numpy matrix, convert it to panda dataframe
    data = np.loadtxt(fileN).T
    data_df = pd.DataFrame(data=data.T, columns=["ylow", "ymid","yhigh","sig0","sigE0","sig1","sigE1","sig2","sigE2","sig3","sigE3","sig4","sigE4","sig5","sigE5","sig6","sigE6"])
    
    return data_df

'''
    f = open(fileN,"r+")
    tab = []
    cols = ['ylow','ymid','yhigh']

    for i in range(7):
        cols.append('sig'+str(i))
        cols.append('sigE'+str(i))
        for l in f:
            if l[0] == '#':  pass
            else:
                b = []

                a = l.split()
                for i in range(len(a)):
                    b.append(float(a[i]))
                tab.append(b)
    
    biny = arange(len(tab)) # array with the bins numbers
    tab = array(tab)
    pd = DataFrame(tab,columns=cols)
    # add "errors"    
    pd['muR'] = nan_to_num( 0.5*(pd['sig1']-pd['sig4'])/pd['sig0']*100 )
    pd['muF'] = nan_to_num( 0.5*(pd['sig2']-pd['sig5'])/pd['sig0']*100 )
    pd['Bin'] = biny
'''

'''
#read file NNLO
#fileN='NNLO_Z.yz_CDF_0908.3914.dat'
fileNNLO='NNLO_only_Wp.ylp_D0_1309.2591_25pt.dat'
#MassLow=int(fileNNLO.split('_')[4])
#MassHigh=int(fileNNLO.split('_')[5])
#print MassLow, MassHigh
pp_nnlo = DataFrame(columns=['sig0','sigE0','muR','muF'])
pd_nnlo = readBinCC(fileNNLO)
pp_nnlo = concat([pp_nnlo,pd_nnlo],ignore_index=True)
print pd_nnlo
p2_nnlo = DataFrame()
l = ['Bin','sig0','sigE0','muR','muF']
for ll in l:
    p2_nnlo[ll] = pp_nnlo[ll]
#p2['sigE0'] = abs(nan_to_num(p2['sigE0']/p2['sig0']*100))
#p2['Sigma'] = p2['sig0']/(MassHigh-MassLow)/2/1000.
p2_nnlo['Stat'] = p2_nnlo['sigE0']


p2_nnlo.to_csv(fileNNLO.replace(".dat",".csv"),sep=" ",index=False)

print(p2_nnlo)

print(type(p2_nnlo['sig0']))

# read file nlo
fileNLO='NNLO_Wp.ylp_D0_1309.2591_25pt.dat'
print("read file nlo "+fileNLO)
pp_nlo = DataFrame(columns=['sig0','sigE0','muR','muF'])
pd_nlo = readBinCC(fileNLO)
pp_nlo = concat([pp_nlo,pd_nlo],ignore_index=True)
print pd_nlo
p2_nlo = DataFrame()
l = ['Bin','sig0','sigE0','muR','muF']
for ll in l:
    p2_nlo[ll] = pp_nlo[ll]
p2_nlo['Stat'] = p2_nlo['sigE0']
p2_nlo.to_csv(fileNNLO.replace(".dat",".csv"),sep=" ",index=False)

#create nlo+nnlo
p2_nlo_nnlo = copy.deepcopy(p2_nlo)
p2_nlo_nnlo['sig0'] = p2_nlo_nnlo['sig0'] + p2_nnlo['sig0']

print(p2_nnlo['sig0'])
print(p2_nlo_nnlo['sig0'])

# Make plots
#plt.errorbar(p2_nlo.index, p2_nlo['sig0'], yerr=p2_nlo['sigE0'])
nlo_plt = plt.errorbar(p2_nlo.index, p2_nlo['sig0'], yerr=p2_nlo['sigE0'], marker = 'o', label='NNLO')
plt.title(fileNLO)

plt.fill_between(p2_nlo['Bin'],  pd_nlo['sig1'], pd_nlo['sig4'], alpha=0.35)
plt.yscale("log")

#p2_nlo['sig0'].plot()
#p2_nlo_nnlo['sig0'].plot()
plt.legend([nlo_plt], ['NNLO'])
plt.show()
#time.sleep(60)
'''


'''
fileNLO = 'NLO_Wm.ylm_D0_1309.2591_25pt.dat'  #'NNLO_Wp.ylp_D0_1309.2591_25pt.dat'
fileNNLO = 'NNLO_Wm.ylm_D0_1309.2591_25pt.dat'  #'NNLO_Wp.ylp_D0_1309.2591_25pt.dat'
print("read file nlo "+fileNNLO)
pd_nnlo = readBinCC(fileNNLO)
pd_nlo = readBinCC(fileNLO)
#plt.errorbar(p2_nlo.index, p2_nlo['sig0'], yerr=p2_nlo['sigE0'])

#plt.errorbar(pd_nnlo['ymid'], pd_nnlo['sig0'], yerr=pd_nnlo['sigE0'], marker = 'o', label='NNLO')
#plt.title(fileNNLO)

plt.errorbar(pd_nlo["ymid"], pd_nlo['sig0'],  yerr=pd_nlo['sigE0'],  color = 'red', label='NNLO', lw=2)
plt.plot(pd_nlo["ymid"], pd_nlo['sig1'], color = 'red', alpha=0.55)
plt.plot(pd_nlo["ymid"], pd_nlo['sig2'], color = 'red', alpha=0.55)
plt.plot(pd_nlo["ymid"], pd_nlo['sig3'], color = 'red', alpha=0.55)
plt.plot(pd_nlo["ymid"], pd_nlo['sig4'], color = 'red', alpha=0.55)
plt.plot(pd_nlo["ymid"], pd_nlo['sig5'], color = 'red', alpha=0.55)
plt.plot(pd_nlo["ymid"], pd_nlo['sig6'], color = 'red', alpha=0.55)

plt.errorbar(pd_nnlo["ymid"], pd_nnlo['sig0'], yerr=pd_nlo['sigE0'], label='NNLO' , lw=2)
plt.plot(pd_nnlo["ymid"], pd_nnlo['sig1'], color= 'blue', alpha=0.55)
plt.plot(pd_nnlo["ymid"], pd_nnlo['sig2'], color= 'blue', alpha=0.55)
plt.plot(pd_nnlo["ymid"], pd_nnlo['sig3'], color= 'blue', alpha=0.55)
plt.plot(pd_nnlo["ymid"], pd_nnlo['sig4'], color= 'blue', alpha=0.55)
plt.plot(pd_nnlo["ymid"], pd_nnlo['sig5'], color= 'blue', alpha=0.55)
plt.plot(pd_nnlo["ymid"], pd_nnlo['sig6'], color= 'blue', alpha=0.55)

plt.legend()
plt.title('Z.abs_yll_mll_116_150_central')
plt.yscale("log")
plt.show()
'''



'''
pd_nnlo.plot(x = 'ymid', y = 'sig0')
pd_nnlo.plot(x = 'ymid', y = 'sig1')
pd_nnlo.plot(x = 'ymid', y = 'sig2')
pd_nnlo.plot(x = 'ymid', y = 'sig3')
pd_nnlo.plot(x = 'ymid', y = 'sig4')
pd_nnlo.plot(x = 'ymid', y = 'sig5')
pd_nnlo.plot(x = 'ymid', y = 'sig6')
'''

#plt.fill_between(pd_nnlo['ymid'],  pd_nnlo['sig5'], pd_nnlo['sig2'], alpha=0.35)



'''
###write .txt with NLO NNLO NNLOmuR2 NNLOmuR05 NNLOmuF2 NNLOmuF05 NNLOmuR2muF2 NNLOmuR05_muF05
process_name = '' # from this built the files names
#change manually the file names, and the output file name later
folder = "./" #"./inputs/ATLAS_7TeV_Z_inclusive/"
file_nlo = folder+'NLO_Wp.ylp_D0_1309.2591_35pt.dat' #file with calculation up to NLO
file_nnlo = folder+'NNLO_Wp.ylp_D0_1309.2591_35pt.dat' #file with calculation up to NNLO

#read the files, get their contents as pandas df
df_nlo = readBinCC(file_nlo)
df_nnlo = readBinCC(file_nnlo)

#add nlo column to nnlo df
df_nnlo["sig0_nlo"] = df_nlo["sig0"]
print(df_nnlo)
#write .txt with: NLO NNLOmuRmuF NNLOmuR2 NNLOmuR05 NNLOmuF2 NNLOmuF05 NNLOmuR2muF2 NNLOmuR05_muF05
#np.savetxt(r'ale_files/Z.yz_CDF_0908.3914.txt', df_nnlo[['sig0_nlo','sig0','sig1','sig4','sig2','sig5','sig3','sig6']].values, fmt='%d')

save_file = "/afs/desy.de/user/g/guidaale/Documents/PhD/dust/xFitter_proj/nnlojetpredictions/ale_files/"

header_str = ["#NLO", "NNLO", "NNLOmuR2", "NNLOmuR05", "NNLOmuF2", "NNLOmuF05", "NNLOmuR2muF2", "NNLOmuR05_muF05"]
df_nnlo[['sig0_nlo','sig0','sig1','sig4','sig2','sig5','sig3','sig6']].to_csv(save_file+'Wp.ylp_D0_1309.2591_35pt.txt', header=header_str, index=None, sep=' ', mode='w')

'''
