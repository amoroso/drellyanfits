# macro to calculate kfactor for cross section (the procedure should be similar for every file)
import pandas as pd
import numpy as np
import time, copy
import matplotlib.pyplot as plt
import convert # macro to read the nnlojets files and convert them into pandas df
import argparse, os

# define envelopes columns
def envelopeColumns(df):
    df = df.assign(envlope_min=lambda d: d[['sig1', 'sig2', 'sig3', 'sig4', 'sig5', 'sig6']].min(1))
    df = df.assign(envlope_max=lambda d: d[['sig1', 'sig2', 'sig3', 'sig4', 'sig5', 'sig6']].max(1))
    return df

def getKFactor(lojet, nlojet, nnlojet, nlofile, muscale='sig0', multipl = True, scale = 1, oname = 'kf.txt', ew = None, outfolder = './'):
    # create out folder
    if (not os.path.exists(outfolder)): # if the path does not exist create it
        os.makedirs(outfolder)
    
    err_col = {'sig0':'sigE0', 'sig1':'sigE1', 'sig2':'sigE2', 'sig3':'sigE3',
               'sig4':'sigE4', 'sig5':'sigE5', 'sig6':'sigE6',} # chose the right col in the nnlojet file with the stat error

    # read the file of theo. prediction and convert it to pandas df
    lo_df = convert.readBinCC(lojet) # leading order from NNLOJET
    nlo_df = convert.readBinCC(nlojet) # NLO from NNLOJET
    nnlo_df = convert.readBinCC(nnlojet) # NNLO from NNLOJET

    nlo_df = envelopeColumns(nlo_df)
    nnlo_df = envelopeColumns(nnlo_df)

    print("LO from NNLOJET")
    print(lo_df)
    print("\n NLO from NNLOJET")
    print(nlo_df)
    print("\n NNLO from NNLOJET")
    print(nnlo_df)

    # read the EW kfactor file
    nlo_ew_kf = None
    nlo_ew_kf_df = None
    if (ew): # if the data file is specified then open it
        nlo_ew_kf = np.loadtxt(ew)
        nlo_ew_kf_df = pd.DataFrame(nlo_ew_kf, columns =['ylow', 'yup', 'kf', 'e_stat']) 
        print("EW KF")
        print(nlo_ew_kf_df)

    # there a factor 10^3 between the two cross sect
    kfactor_nnlojet = pd.DataFrame()
    kfactor_nnlojet['kf'] = nnlo_df[muscale] / nlo_df['sig0']
    kfactor_nnlojet['e_stat'] = kfactor_nnlojet['kf']*((nnlo_df[err_col[muscale]]/nnlo_df[muscale])**2 + (nlo_df['sigE0']/nlo_df['sig0'])**2)**0.5 # statistical error propagation

    kfactor_lojet = pd.DataFrame()
    kfactor_lojet['kf'] = nnlo_df[muscale] / lo_df['sig0'] # NNLO_QCD/LO_QCD
    kfactor_lojet['e_stat'] = kfactor_lojet['kf']*((nnlo_df[err_col[muscale]]/nnlo_df[muscale])**2 + (lo_df['sigE0']/lo_df['sig0'])**2)**0.5 #

    # path and name of the file containing the theory prediction on the grid
    file_fittedresults = "/afs/desy.de/user/g/guidaale/Documents/PhD/dust/xFitter_proj/xfitter/myrun/profile/out-d0-NNPDF31_nnlo_as_0118/fittedresults.txt"

    # read and convert to df
    # the column with thorig is 6
    fittedresults_df = pd.read_csv(nlofile, sep="\s+", header=None, skiprows=[0,1,2,3,4])
    #print(nnlo_df)
    print("NLO from GRID")
    print(fittedresults_df[6])

    # calculate the NNLO QCD, NLO EW term (additive EW)
    nnloqcd_nloew_df = pd.DataFrame()
    if(ew): # if the ew kf file is specified calculate the KF including the EW KF
        if(multipl): # multiply by the EW KF
            nnloqcd_nloew_df['sigma'] = nnlo_df[muscale] * nlo_ew_kf_df['kf']
            nnloqcd_nloew_df['e_stat'] = nnloqcd_nloew_df['sigma']*((nnlo_df[err_col[muscale]]/nnlo_df[muscale])**2 + (nlo_ew_kf_df['e_stat']/nlo_ew_kf_df['kf'])**2)**0.5
        else: # use the EW kfactor in additive way
            nnloqcd_nloew_df['sigma'] = nnlo_df[muscale] * (1+(nlo_ew_kf_df['kf']-1)/kfactor_lojet['kf'])
            nnloqcd_nloew_df['e_stat'] = ((1+(nlo_ew_kf_df['kf']-1)/kfactor_lojet['kf'])**2 * nnlo_df[err_col[muscale]]**2 +
                                          (nlo_ew_kf_df['kf']/kfactor_lojet['kf'])**2 * nlo_ew_kf_df['e_stat']**2 +
                                          (nnlo_df[muscale]*(nlo_ew_kf_df['kf']-1)/kfactor_lojet['kf']**2)**2 * kfactor_lojet['e_stat']**2)**0.5
    else: # else consider only the nnlo qcd
        nnloqcd_nloew_df['sigma'] =  nnlo_df[muscale]
        nnloqcd_nloew_df['e_stat'] = nnlo_df[err_col[muscale]]

    #fittedresults_kf_df = pd.read_csv(file_fittedresults_kf, sep=" ", header=None, skiprows=[0,1,2,3,4])
    # calculate kfactor  using the nlo present in the grid
    # there's a factor 10^3 between the two cross sect
    kfactor_df=pd.DataFrame()
    # Calculate the kfactor as NNLO_QCD+NLO_EW/NLO_QCD_GRID
    kfactor_df["kf"] = (nnloqcd_nloew_df['sigma']*scale)/(fittedresults_df[6])
    #kfactor_df["kf"] = (nnlo_df[muscale]/1000.)/(fittedresults_df[6]) # this the kfactor to save to file
    kfactor_df["e_stat"]=(nnloqcd_nloew_df['e_stat']*scale)/fittedresults_df[6] # progate statistical error in the calculation of the NNLO order to the kfactor
    
    print("kfactor nnlojet/grid")
    print(kfactor_df)

    print("kfactor nnlojet nnlo/nlo")
    print(kfactor_nnlojet)
    # some check plots

    plt.figure()
    plt.plot(kfactor_df.index, kfactor_df['kf'], label='kfactor nnlojet/grid')
    plt.plot(kfactor_df.index, kfactor_nnlojet['kf'], label='kfactor nnlojet/nlojet')
    plt.title(nnlojet)
    plt.legend(loc=4)
    plt.show()

    #plt.figure()
    plt.plot((fittedresults_df[0]+fittedresults_df[1])/2, fittedresults_df[6], label='xsec grid')
    plt.title(nlojet)
    plt.legend(loc=4)
    plt.show()

    # save kfactor to file in the format
    # ylow yhigh kfactor e_stat
    pd.concat( [fittedresults_df[[0,1]],kfactor_df ], axis=1).to_csv(os.path.join(outfolder,oname), header=None, index=None, sep=' ', mode='w')


if __name__ == "__main__":
    #parsing argument
    parser = argparse.ArgumentParser(description='Calculate KFactor as NNLO/NLO', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-loj', '--lojet', help = 'LO NLOJET data file')
    parser.add_argument('-nloj', '--nlojet', help = 'NLO NLOJET data file')
    parser.add_argument('-nnloj', '--nnlojet', help = 'NNLO NLOJEt data file')
    parser.add_argument('-nlo', '--nlofile', help = 'NLO data file from the grid (fitted result file)')
    parser.add_argument('-mus', '--muscale', 
                        help = '''chose wich muF, muR scale variation use for the nnlojets prediction
                        legend
                        sig0 muf = mln mur = mln
                        sig1 muf = mln mur = 2.0 * mln
                        sig2 muf = 2.0 * mln mur = mln
                        sig3 muf = 2.0 mln mur = 2.0 mln
                        sig4 muf = mln mur = 0.5 * mln
                        sig5 muf = 0.5 * mln mur = mln
                        sig6 muf = 0.5 mln mur = 0.5 mln
                        ''',
                        choices = ['sig0','sig1','sig2','sig3','sig4','sig5','sig6'])
    parser.add_argument("-ew","--ew", help="NLO electroweak kfactor")
    parser.add_argument("-mul","--multipl", help="The electroweak kfactor are taken into account multiplicatively, (the default is the additive)", action = "store_true")
    parser.add_argument('-s', '--scale', help = 'scale the nnlojet data by this factor', type = float, default=1.)
    parser.add_argument('-o', '--oname', help = 'output file name', default= 'kf.txt')
    parser.add_argument('-ofolder', '--outfolder', help = "out folder", default = './')
    args = parser.parse_args()

    
    # input files names
    file_name = "Z.yz_CDF_0908.3914.dat"##"Wm.ylm_D0_1309.2591_35pt.dat"
    file_nlo = "NLO_"+file_name
    file_nnlo = "NNLO_"+file_name

    # call the function to save the kfactor
    getKFactor(lojet = args.lojet, nlojet = args.nlojet, nnlojet=args.nnlojet, nlofile=args.nlofile,
               muscale=args.muscale, multipl = args.multipl, scale = args.scale, oname = args.oname, ew = args.ew,
               outfolder = args.outfolder)
