#!/usr/bin/env python3
# plotting macro based on the NNLOJET plotting macro

import numpy as np
import matplotlib.pyplot as plt
#import iosevka_plot
import pandas as pd
import os, argparse, convert
#iosevka_plot.setup()

#import atlas_mpl_style as ampl
#ampl.use_atlas_style()
from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
#rc('text', usetex=True)


datadir = "8TeV_wpm_EWcorr/" #"8TeV_wpm_EWcorr/"
orders = ["NNLO", "NNLO+EW"]
colours = ["blue", "yellow", "green", "red", "aqua"]#"yellow"]  #, "red"]
normorder = "NNLO"
scale_err_scheme = "semicorr"  # semicorr, uncorr, corr
err_scheme_name = {"semicorr": "Semi-correlated Errors",
                   "uncorr": "Uncorrelated Errors",
                   "corr": "Fully Correlated Errors"}


def load_ATLAS(fname, norm=False):
    data = np.loadtxt(fname).T
    errors = np.sqrt(np.sum(data[3:]**2, axis=0))*data[2]/100 # the error from the input file are in percent
    midpoints = 0.5*(data[0]+data[1])
    vals = data[2]
    if norm:
        vals = vals/(data[1]-data[0])
        errors = errors/(data[1]-data[0])
    return midpoints, errors, vals


def get_histxy(xlo, xhi, y):
    xvals = []
    for x in zip(xlo, xhi):
        xvals += [x[0], x[1]]
    yvals = []
    for yval in y:
        yvals += [yval, yval]
    return np.array(xvals), np.array(yvals)


scales = [(1, 1), (1, 2), (2, 1), (2, 2), (1, 0.5), (0.5, 1), (0.5, 0.5)]


def scale_combo_allowed(id1, id2):
    scale1 = scales[id1]
    scale2 = scales[id2]
    up = 2.01
    do = 0.499
    if scale1[0]/scale2[0] >= up or scale1[0]/scale2[0] < do:
        return False
    if scale1[1]/scale2[0] >= up or scale1[1]/scale2[0] < do:
        return False
    if scale1[0]/scale2[1] >= up or scale1[0]/scale2[1] < do:
        return False
    if scale1[1]/scale2[1] >= up or scale1[1]/scale2[1] < do:
        return False
    return True


def get_semicorr(weight, norm):
    results = []
    for wgtrow, normrow in zip(weight.T, norm.T):
        scalevars = []
        for idwgt, wgtscale in enumerate(wgtrow):
            for idnorm, normscale in enumerate(normrow):
                if scale_combo_allowed(idwgt, idnorm):
                    scalevars.append(wgtscale/normscale)
        results.append(scalevars)
    results = np.array(results)
    return results


def get_uncorr(weight, norm):
    results = []
    for wgtrow, normrow in zip(weight.T, norm.T):
        scalevars = []
        for idwgt, wgtscale in enumerate(wgtrow):
            for idnorm, normscale in enumerate(normrow):
                scalevars.append(wgtscale/normscale)
        results.append(scalevars)
    results = np.array(results)
    return results


def get_corr(weight, norm):
    return weight/norm


get_scale = {"semicorr": get_semicorr,
             "uncorr": get_uncorr,
             "corr": get_corr}


def gen_asym(order):
    wm = np.loadtxt(os.path.join(datadir,"Wm.8TeV.{0}.muon_eta.ALL.dat".format(order))).T
    wm_cent = wm[3]
    wp = np.loadtxt(os.path.join(datadir,
                                 "Wp.8TeV.{0}.muon_eta.ALL.dat".format(order))).T
    wp_cent = wp[3]
    cent = (wp_cent-wm_cent)/(wp_cent+wm_cent)
    xlo = wp[0]
    xhi = wp[2]
    mids = wp[1]
    xvals, yvals = get_histxy(xlo, xhi, cent)
    scale_errors = get_scale[scale_err_scheme](wp[3::2]-wm[3::2], wp[3::2]+wm[3::2])
    if scale_err_scheme == "corr":
        ax_ = 0
    else:
        ax_ = 1
    _, scale_up = get_histxy(xlo, xhi, np.max(scale_errors, axis=ax_))
    _, scale_do = get_histxy(xlo, xhi, np.min(scale_errors, axis=ax_))
    return xvals, yvals, mids, scale_up, scale_do, cent


def gen_nnlojet(order, boson):
    data=None
    if(order=="NNLO+EW"):# the EW correction is applied later to the NNLO file, retrieve this one
        data = np.loadtxt(os.path.join(datadir,"{1}.8TeV.{0}.muon_eta.ALL.dat".format("NNLO", boson))).T
    else:
        data = np.loadtxt(os.path.join(datadir,"{1}.8TeV.{0}.muon_eta.ALL.dat".format(order, boson))).T
    xlo = data[0]
    xhi = data[2]#
    binwidth = 0.241
    cent = data[3]/1000*binwidth
    mids = data[1]
    xvals, yvals = get_histxy(xlo, xhi, cent)
    _, scale_up = get_histxy(xlo, xhi, np.max(data[3::2], axis=0)/1000*binwidth)
    _, scale_do = get_histxy(xlo, xhi, np.min(data[3::2], axis=0)/1000*binwidth)
    
    if (order=="NNLO+EW" and boson == "Wm"): # apply the EW kfactor
        # kf file format 
        # bin_down bin_up KF err_KF
        ew_kf = np.loadtxt(os.path.join(datadir,'renesance_etamuminus_KF.txt')).T # get the kfactor from the file
        _, ew_kf_hist = get_histxy(xlo, xhi, ew_kf[2])
        print(ew_kf_hist)
        print(scale_up)
        cent = cent * ew_kf[2]
        scale_up *= ew_kf_hist
        scale_do *= ew_kf_hist
        yvals *= ew_kf_hist

    return xvals, yvals, mids, scale_up, scale_do, cent

# define envelope columns, do the envelope between the scale variations
def envelopeColumns(df):
    df = df.assign(envelope_min=lambda d: d[['sig1', 'sig2', 'sig3', 'sig4', 'sig5', 'sig6']].min(1))
    df = df.assign(envelope_max=lambda d: d[['sig1', 'sig2', 'sig3', 'sig4', 'sig5', 'sig6']].max(1))
    return df

def getNNLOJET(list_file, scale = 1.): # given a list of files return a list of dataframe with the file content, it produce also the envelope columns of the scale variation
    nlojet_df_list = []
    for file_entry in list_file:
        nlojet_df = convert.readBinCC(file_entry)
        # scale the entries
        nlojet_df[nlojet_df.columns[3:22]] = scale * nlojet_df[nlojet_df.columns[3:22]]
        nlojet_df = envelopeColumns(nlojet_df) # produce the 7 scale variation envelope
        nlojet_df_list.append(nlojet_df)
    return nlojet_df_list

def getData(list_file, isPercent = False): # return a list of df with bins data value and error, it reads the fitted results file from xfitter
    data_df_list = []
    for file_entry in list_file:
        data_df = pd.read_csv(file_entry, sep="\s+", header=None, skiprows=[0,1,2,3,4])
        # select only the columns with bins values, data values and total error values
        data_df = data_df[[0,1,3,5]]
        # set a name for the columns
        data_df = data_df.rename(columns={0:'bin_low',1:'bin_high',3:'data',5:'err'})
        data_df["bin_mid"] = (data_df["bin_high"]+data_df['bin_low'])/2
        if isPercent: # if the error are in percent, trasform them to absolute error
            data_df['err'] = data_df['err']*data_df['data']/100.
        # append the data frame to the returned list
        data_df_list.append(data_df)
    return data_df_list
        
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Draw plot')
    parser.add_argument('-fd', '--fdata', nargs='+', help = 'data file')
    parser.add_argument('-fg', '--fgrid', nargs='+',  help = 'mc file from the grid')
    parser.add_argument('-fjet','--fnlojet', nargs='+', help = "File nnlojet type, insert also the legend label for this entry. Usage '--fnlojet file_path1:legend_label1 file_path2:legend_label2 ...'") #type=argparse.FileType('r'), 
    parser.add_argument('-fwjet','--fwasyjet', nargs=2, help = "W plus and W minus files to compute and plot the W charge asym. Wplus should be specified first") #type=argparse.FileType('r'), 
    parser.add_argument("-t","--title", help= "plot title", nargs ='?', default='',  const = "") 
    parser.add_argument("-xl","--xlabel", help= "Label for the xaxis",
                        default="$y_{ll}$")
    parser.add_argument("-yl","--ylabel", help= "Label for the upper plot yaxis",
                        default="$d\sigma / dy_{ll} \quad [pb]$")
    parser.add_argument("-rl","--rlabel", help= "Label for the ratio plot yaxis",
                        default='Data/MC')
    parser.add_argument("-s","--scale", help= "multiplying factor for nnlojet prediction", type=float, default=1.) 
    parser.add_argument("--norm", help=" Normalize MC to the total cross section", action="store_true")
    parser.add_argument("--out_name", "-o", help= "Name of output pdf file", default="plot.pdf") 
    parser.add_argument("--annotate", "-a", help = "Write this on the top right", nargs='+',
                        default = "$ATLAS$ \n $\sqrt{s}=8$ TeV \n $Z \qquad 46<m_{\mu\mu}<66\quad GeV$")
    parser.add_argument("--percent", "-p", help = "Specify if the error in data are in percent", action="store_true")
    parser.add_argument("--add_line", "-add", help="Add line to annotate on the plot", default="")
    parser.add_argument('-tx', '--text_posx', help='x position of the annotate text', default=0.1, type=float),
    parser.add_argument('-ty', '--text_posy', help='y position of the annotate text', default=0.1, type=float),
    parser.add_argument('-xr', '--xrange', help = 'xaxis range', default=[0,2.4], type=float, nargs='+')
    args = parser.parse_args()
    #args.xlabel=' '.join(args.xlabel)
    #args.ylabel=' '.join(args.ylabel)
    # the input has the form 'file_name:label_for_this_file'
    nlojet_list=[]
    nlojet_label_list = []
    [nlojet_list.append(i.split(":")[0]) for i in args.fnlojet]
    [nlojet_label_list.append(i.split(":")[1] if ":" in i else " ") for i in args.fnlojet]
    print(nlojet_list)
    print(nlojet_label_list)
    
    # retrieve nnlojet files
    nlojet_df_list = [] # save the nlojet in a list and plot them later
    if(args.fnlojet): # check that this option has been called
        nlojet_df_list = getNNLOJET(nlojet_list, scale = args.scale)

    # retrieve data files
    data_df_list = []
    if(args.fdata):
        data_df_list= getData(args.fdata, isPercent = args.percent)
        
    # define the plot
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(9, 10),
                             sharex=True,
                             gridspec_kw={'height_ratios': [3, 1]})
    ax = axes[0]
    ax2 = axes[1]
    
    # plot prediction nnlojet
    for idx, df in enumerate(nlojet_df_list):
        # generate arrays to draw the points like an histogram
        x, y = get_histxy(df['ylow'], df['yhigh'],df['sig0'])
        _, yup = get_histxy(df['ylow'], df['yhigh'],df['envelope_max'])
        _, ydown = get_histxy(df['ylow'], df['yhigh'],df['envelope_min'])
        # plot the central value and the 7 scale variation as envelope
        ax.plot(x, y, color=colours[idx], label=nlojet_label_list[idx], linewidth=1)
        ax.fill_between(x, ydown, yup, alpha=0.3, color=colours[idx])
        #plot ratio to data
        # get the arrays to draw the points like an histogram
        _, y_r = get_histxy(df['ylow'], df['yhigh'], data_df_list[0]['data']/df['sig0'])
        _, yup_r = get_histxy(df['ylow'], df['yhigh'], data_df_list[0]['data']/df['envelope_max'])
        _, ydown_r = get_histxy(df['ylow'], df['yhigh'], data_df_list[0]['data']/df['envelope_min'])
        # again get the 
        ax2.plot(x, y_r, color=colours[idx], label=nlojet_label_list[idx], linewidth=1)
        ax2.fill_between(x, ydown_r, yup_r, alpha=0.3,color=colours[idx])
    

    #plot the data
    for idx, data_df in enumerate(data_df_list):
        ax.errorbar(data_df['bin_mid'], data_df['data'], yerr=data_df['err'], color="black",
                    linewidth=2, markersize=5, marker="o", fmt="none", label="Data")
        ax2.errorbar(data_df['bin_mid'], data_df['data']/data_df['data'], yerr=data_df['err']/data_df['data'],
                     color="black", linewidth=2, markersize=5, marker="o", fmt="none")
        
    # set plot atributes (axis names, title, etc)
    ax2.set_xlabel(r'{}'.format(args.xlabel))
    ax.set_ylabel(r'{}'.format(args.ylabel),
                  weight="light")
    ax2.set_ylabel(args.rlabel, weight="light")
    ax.set_xlim(args.xrange)
    ax.legend()
    ax.grid()
    ax2.grid()
    ax.annotate('NNLOJET', xy=(0.02, 1), xycoords='axes fraction',
                fontsize=int(26), xytext=(0, 0),
                textcoords='offset points', fontweight='semibold',
                style='italic', ha='left', va='bottom')
    #ax.annotate(args.annotate, xy=(0.97, 1.06), xycoords='axes fraction',
     #           fontsize=int(23), xytext=(0, 0),
      #          textcoords='offset points', fontweight='semibold',
       #         style='italic', ha='right', va='bottom',)
                #bbox=dict(boxstyle='round,pad=0.2', fc='yellow', alpha=0.3))
    #ann_text = args.annotate.encode('unicode_escape')
    #print(type(args.annotate))
    #print('\n' in args.annotate)
    #args.annotate.replace('\n', '\\n')
    #print("ATLAS  $\sqrt{s}=7 TeV Z$ \n $ low mass \quad CC$")
    print(' '.join(args.annotate))
    ann = ' '.join(args.annotate)
    #ann.replace('\n', '\\n')
    print(r"{}".format(ann))
    #print("ATLAS  $\sqrt{s}=7 TeV Z$ \n $ low mass \quad CC$"==args.annotate)
    ax.annotate(r"{}".format(ann),  xy=(args.text_posx, args.text_posy) , xycoords='axes fraction',
                fontsize=int(23),
                fontweight='semibold',
                style='italic',)# ha='right', va='bottom',)
                #bbox=dict(boxstyle='round,pad=0.2', fc='yellow', alpha=0.3))
    #ax.annotate(args.annotate_l2,  xy=(args.text_posx, args.text_posy) , xycoords='axes fraction',
    #            fontsize=int(23),
    #            fontweight='semibold',
    #            style='italic',)# ha='right', va='bottom',)
                #bbox=dict(boxstyle='round,pad=0.2', fc='yellow', alpha=0.3))
    
    plt.tight_layout()

    #save the plot to file
    plt.savefig(args.out_name, bbox_inches='tight')
    
    plt.show()
