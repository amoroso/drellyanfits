# apply the ew kf factor to the NNLOjet prediction

import pandas as pd
import numpy as np
import time, copy
import matplotlib.pyplot as plt
import convert # macro to read the files and convert them into panda df
import argparse

# define envelopes columns
def envelopeColumns(df):
    df = df.assign(envlope_min=lambda d: d[['sig1', 'sig2', 'sig3', 'sig4', 'sig5', 'sig6']].min(1))
    df = df.assign(envlope_max=lambda d: d[['sig1', 'sig2', 'sig3', 'sig4', 'sig5', 'sig6']].max(1))
    return df

#def applyEWkfMul ()

#parsing argument
parser = argparse.ArgumentParser(description='Calculate KFactor as NNLO/NLO')
parser.add_argument('-f_nnlo', '--file_nnlo', help = 'NNLOJET data file', required=True)
parser.add_argument('-f_lo', '--file_lo', help = 'LO prediciton from NNLOJET')
parser.add_argument("-ew","--ew", help="NLO electroweak kfactor",required=True)

# either choose mul or add
mutex_group = parser.add_mutually_exclusive_group()
mutex_group.add_argument("-mul", "--mul", action="store_const", dest="mutex", const="mul",
                         help="The electroweak kfactor are taken into account multiplicatively")
mutex_group.add_argument("-add", "--add", action="store_const", dest="mutex", const="add",
                         help="The electroweak kfactor are taken into account additively")
parser.set_defaults(mutex='mul') # set multiplicative as default

parser.add_argument('-s', '--scale', help = 'scale the nnlojet data by this factor', type = float, default=1.)
parser.add_argument('-o', '--oname', help = 'output file name', default= 'ew.txt')

args = parser.parse_args()


nlojet_df = convert.readBinCC(args.file_nnlo) # read file from NNLOJET
print("NNLOJET FILE", nlojet_df)


# read the EW kfactor file
nlo_ew_kf = None
nlo_ew_kf_df = None
nlo_ew_kf = np.loadtxt(args.ew) # read as numpy array and convert to pandas df
nlo_ew_kf_df = pd.DataFrame(nlo_ew_kf, columns =['ylow', 'yup', 'kf', 'e_stat']) 
print("EW KF")
print(nlo_ew_kf_df)

nnloqcd_nloew_df = copy.deepcopy(nlojet_df)
if args.mutex == 'mul': # apply the EW KF in the multiplicative way
    # apply the ew kf and propagate the stat error for each scale variation
    list_col = [["sig0","sigE0"],["sig1","sigE1"],["sig2","sigE2"],["sig3","sigE3"],["sig4","sigE4"],["sig5","sigE5"],["sig6","sigE6"],]
    for col_name in list_col:
        nnloqcd_nloew_df[col_name[0]] = nlojet_df[col_name[0]] * nlo_ew_kf_df['kf']
        # propagate the statistical error
        nnloqcd_nloew_df[col_name[1]] = nnloqcd_nloew_df[col_name[0]]*(
            (nlojet_df[col_name[1]]/nlojet_df[col_name[0]])**2 +
            (nlo_ew_kf_df['e_stat']/nlo_ew_kf_df['kf'])**2)**0.5

if args.mutex == 'add': # apply the EW KF in the additive way
    # assume the the EW kf are calculated at LO in QCD,
    # use the formul sigma_NNLOQCD_NLOEW = sigma_NNLOQCD_LOEW * (1 + (KF_EW - 1)/KF_QCD)
    # where KF_QCD = sigma_NNLOQCD / sigma_LOQCD
    lojet_df = convert.readBinCC(args.file_lo) # read the leading order prediction (used for the additive kf_ew application)
    print("LO nlojet file", lojet_df)

    list_col_val = ["sig0","sig1","sig2","sig3","sig4","sig5","sig6"]
    list_col_err = ["sigE0","sigE1","sigE2","sigE3","sigE4","sigE5","sigE6"]
    list_col = [["sig0","sigE0"],["sig1","sigE1"],["sig2","sigE2"],["sig3","sigE3"],["sig4","sigE4"],["sig5","sigE5"],["sig6","sigE6"],]
    # calculate kf_qcd = sigma_nnloqcd / sigma_loqcd
    kf_qcd = nlojet_df[list_col_val]/lojet_df[list_col_val]
    # propagate stat error
    kf_qcd[list_col_err] = ( (nlojet_df[list_col_err]/nlojet_df[list_col_val].values)**2 +
                             (lojet_df[list_col_err]/lojet_df[list_col_val].values)**2)
    # calculate the NNLO+EW prediction
    for col_name in list_col:
        nnloqcd_nloew_df[col_name[0]] = nlojet_df[col_name[0]] * ( (1 + nlo_ew_kf_df['kf'])/kf_qcd[col_name[0]])
        # propagate the statistical error
        nnloqcd_nloew_df[col_name[1]] = ((1+(nlo_ew_kf_df['kf']-1)/kf_qcd[col_name[0]])**2 * nlojet_df[col_name[0]]**2 +
                                         (nlo_ew_kf_df['kf']/kf_qcd[col_name[0]])**2 * nlo_ew_kf_df['e_stat']**2 +
                                         (nlojet_df['sig0']*(nlo_ew_kf_df['kf']-1)/kf_qcd[col_name[0]]**2)**2 *
                                         kf_qcd[col_name[1]]**2)**0.5
        
    print("kf qcd")
    print(kf_qcd)
    print(type(kf_qcd))
        
print(nnloqcd_nloew_df)

# control plot
plt.figure()
plt.errorbar(nlojet_df['ymid'], nlojet_df['sig0'],yerr=nlojet_df['sigE0'], label='NNLOJET')
plt.errorbar(nnloqcd_nloew_df['ymid'], nnloqcd_nloew_df['sig0'],yerr=nnloqcd_nloew_df['sigE0'], label='NNLOJET +EW')
#plt.plot(kfactor_df.index, kfactor_nnlojet['kf'], label='kfactor nnlojet/nlojet')
plt.title(args.file_nnlo)
plt.legend(loc=4)
plt.show()

#save to file
# start the header with #, because the file will be read with numpy
header_tag = ["#ylow","ymid","yhigh","sig0","sigE0","sig1","sigE1","sig2","sigE2","sig3","sigE3","sig4","sigE4","sig5","sigE5","sig6","sigE6",]
header = ["ylow","ymid","yhigh","sig0","sigE0","sig1","sigE1","sig2","sigE2","sig3","sigE3","sig4","sigE4","sig5","sigE5","sig6","sigE6",]
nnloqcd_nloew_df[header].to_csv(args.oname, header=header_tag, index=None, sep=' ', mode='w')
