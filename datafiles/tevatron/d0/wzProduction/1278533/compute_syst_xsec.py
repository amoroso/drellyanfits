# Calculate the systematic uncertainties on the D0 y Z cross section
import pandas as pd
import math

def weigthAverage(values, values_err): # return the average of the values column weighted with value_err column
    return 1/((values_err)**-2).sum() * (values/values_err**2).sum()

def weightError(err): # return the error for the weighted average, 'err' is a dataframe column
    return math.sqrt(1/((err**-2).sum()))

def xsecNorm (N_obs, N_bg, eA, bin_size) : # calculate the normalized cross section. The arguments are pandas.Series
    xsec = (N_obs -  N_bg)/(2*bin_size * eA)
    norm = ((N_obs -  N_bg)/(eA)).sum()
    return xsec/norm

#def xsecNorm_avg (N_obs, N_bg, eA, eA_e, bin_size) : # calculate the normalized cross section. The arguments are pandas.Series
#    norm = (N_obs.sum() - N_bg.sum())/weigthAverage(eA, eA_e)  # calculate the weighted average on eff*Acceptance
#    xsec = (N_obs -  N_bg)/(bin_size * eA)
#    #norm = xsec.sum()
#    return 0.5*xsec/norm

def propUnc (N_obs, N_bg, eA, eA_e, bin_size) : # propagate uncertanties
    e_eA_avg = weightError(eA_e)
    #norm = weigthAverage(eA, eA_e) / (N_obs.sum() - N_bg.sum()) # calculate the weighted average on eff*Acceptance
    norm = ((N_obs -  N_bg)/(eA)).sum()
    #sum_err_square = (0.5*1/(N_obs.sum() - N_bg.sum())*xsec)**2 * e_eA_avg**2 + (0.5*norm * (N_obs -  N_bg)/(bin_size * eA**2))**2 * eA_e**2
    sum_err_square =  (norm**-1 * (N_obs -  N_bg)/(bin_size*2 * eA**2))**2 * eA_e**2 # + (norm**-2 * (N_obs -  N_bg)/( eA**2))**2 * eA_e**2
    return  sum_err_square**0.5

def addSquare (*columns): # add in square the columns in input
    square_sum = 0
    for col in columns:
        square_sum += (col**2)
    return square_sum**0.5

# read the tables
xsec_results = pd.read_csv('xsec_results_table.txt', sep=" ")
eA_results = pd.read_csv('eA_results_table.txt', sep = " ")

print(xsec_results.to_latex(index = False))
print(eA_results.to_latex(index = False))

my_results = pd.DataFrame()

# define bin size column, the bin size is 0.1 for all (27) bins except the last one that is 0.2
n_bins = pd.Series(range (0, 28)) # bin index
array_bin_size = 27 * [0.1]
array_bin_size.append(0.2)
y_size = pd.Series(array_bin_size)
y_min = pd.Series(28*[])
y_max = pd.Series(28*[])
print(y_size[0])
# def up and low edge for the bins
for i in range(0, len(y_size)):
    if (i ==0):
        y_min[i] = 0
        y_max[i] = y_size[i]
    else:
        y_min[i] = y_min[i-1] + y_size[i]
        y_max[i] = y_max[i-1] + y_size[i]

my_results['y_min'] = y_min
my_results['y_max'] = y_max
my_results['xsec_norm'] = xsec_results['xsec_norm']
#my_results['xsec_norm'] = xsecNorm(xsec_results['sg_events'], xsec_results['bg_events'], eA_results['e*A'], y_size )
eA_syst_tot = addSquare(eA_results['e_method'], eA_results['e_Escale'] , eA_results['e_PDF'] , eA_results['e_ymodeling'], eA_results['e_stat']) # tot syst err on e*A
my_results['e_stat'] = xsec_results['e_stat']
my_results['e_syst'] = xsec_results['e_syst']
#my_results['e_syst'] = propUnc(xsec_results['sg_events'], xsec_results['bg_events'], eA_results['e*A'], eA_syst_tot, y_size )
#my_results['diff_xsec%'] = (xsec_results['xsec_norm'] - my_results['xsec_norm'])/(xsec_results['xsec_norm'])*100 
#my_results['diff_e_syst%'] = (xsec_results['e_syst'] - my_results['e_syst'])/xsec_results['e_syst']*100 
my_results['e_syst_sfstat'] = propUnc(xsec_results['sg_events'], xsec_results['bg_events'], eA_results['e*A'], eA_results['e_stat'], y_size )
my_results['e_syst_method']  = propUnc(xsec_results['sg_events'], xsec_results['bg_events'], eA_results['e*A'], eA_results['e_method'], y_size )
my_results['e_syst_Escale']  = propUnc(xsec_results['sg_events'], xsec_results['bg_events'], eA_results['e*A'], eA_results['e_Escale'], y_size )
my_results['e_syst_PDF']  = propUnc(xsec_results['sg_events'], xsec_results['bg_events'], eA_results['e*A'], eA_results['e_PDF'], y_size )
my_results['e_syst_model']  = propUnc(xsec_results['sg_events'], xsec_results['bg_events'], eA_results['e*A'], eA_results['e_ymodeling'], y_size )
my_results['*flag'] = 28*[1]
my_results.at[ 27, '*flag']= 0

#print to xfitter format



print (my_results[['*flag','y_min', 'y_max','xsec_norm','e_stat', 'e_syst_sfstat', 'e_syst_method','e_syst_Escale','e_syst_PDF','e_syst_model']].round(4).to_string(index=False) )#.to_latex(index = False))
