# copy the file multiple times and substitute the string
import os
from shutil import copyfile

opt_map_list_zpeakcc = [{'filei':'../zypeak_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cc-kf_nnlojet_muR1_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cc_muR1_muF1.txt'},
                        
                        {'filei':'../zypeak_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cc-kf_nnlojet_muR2_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cc_muR2_muF1.txt'},
                        
                        {'filei':'../zypeak_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cc-kf_nnlojet_muR1_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cc_muR1_muF2.txt'},
                    
                        {'filei':'../zypeak_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cc-kf_nnlojet_muR2_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cc_muR2_muF2.txt'},
                
                        {'filei':'../zypeak_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cc-kf_nnlojet_muR05_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cc_muR05_muF1.txt'},
                        
                        {'filei':'../zypeak_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cc-kf_nnlojet_muR1_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cc_muR1_muF05.txt'},
                        
                        {'filei':'../zypeak_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cc-kf_nnlojet_muR05_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cc_muR05_muF05.txt'},
]


opt_map_list_zpeakcf = [{'filei':'../zypeak_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cf-kf_nnlojet_muR1_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cf_muR1_muF1.txt'},
                        
                        {'filei':'../zypeak_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cf-kf_nnlojet_muR2_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cf_muR2_muF1.txt'},
                        
                        {'filei':'../zypeak_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cf-kf_nnlojet_muR1_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cf_muR1_muF2.txt'},
                    
                        {'filei':'../zypeak_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cf-kf_nnlojet_muR2_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cf_muR2_muF2.txt'},
                
                        {'filei':'../zypeak_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cf-kf_nnlojet_muR05_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cf_muR05_muF1.txt'},
                        
                        {'filei':'../zypeak_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cf-kf_nnlojet_muR1_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cf_muR1_muF05.txt'},
                        
                        {'filei':'../zypeak_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zypeak_cf-kf_nnlojet_muR05_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zpeak_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zypeak_cf_muR05_muF05.txt'},
]


opt_map_list_zhighcc = [{'filei':'../zyhigh_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cc-kf_nnlojet_muR1_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cc_muR1_muF1.txt'},
                        
                        {'filei':'../zyhigh_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cc-kf_nnlojet_muR2_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cc_muR2_muF1.txt'},
                    
                        {'filei':'../zyhigh_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cc-kf_nnlojet_muR1_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cc_muR1_muF2.txt'},
                        
                        {'filei':'../zyhigh_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cc-kf_nnlojet_muR2_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cc_muR2_muF2.txt'},
                        
                        {'filei':'../zyhigh_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cc-kf_nnlojet_muR05_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cc_muR05_muF1.txt'},
                        
                        {'filei':'../zyhigh_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cc-kf_nnlojet_muR1_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cc_muR1_muF05.txt'},
                        
                        {'filei':'../zyhigh_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cc-kf_nnlojet_muR05_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cc_muR05_muF05.txt'},
]

opt_map_list_zhighcf = [{'filei':'../zyhigh_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cf-kf_nnlojet_muR1_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cf_muR1_muF1.txt'},
                        
                        {'filei':'../zyhigh_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cf-kf_nnlojet_muR2_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cf_muR2_muF1.txt'},
                    
                        {'filei':'../zyhigh_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cf-kf_nnlojet_muR1_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cf_muR1_muF2.txt'},
                        
                        {'filei':'../zyhigh_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cf-kf_nnlojet_muR2_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cf_muR2_muF2.txt'},
                        
                        {'filei':'../zyhigh_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cf-kf_nnlojet_muR05_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cf_muR05_muF1.txt'},
                        
                        {'filei':'../zyhigh_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cf-kf_nnlojet_muR1_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cf_muR1_muF05.txt'},
                        
                        {'filei':'../zyhigh_cf-kf_nnlojet-thexp.dat',
                         'fileo':'./zyhigh_cf-kf_nnlojet_muR05_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zhigh_cf.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zyhigh_cf_muR05_muF05.txt'},
]

opt_map_list_zlowcc = [{'filei':'../zylow_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zylow_cc-kf_nnlojet_muR1_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zlow_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zylow_cc_muR1_muF1.txt'},
                        
                        {'filei':'../zylow_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zylow_cc-kf_nnlojet_muR2_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zlow_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zylow_cc_muR2_muF1.txt'},
                    
                        {'filei':'../zylow_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zylow_cc-kf_nnlojet_muR1_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zlow_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zylow_cc_muR1_muF2.txt'},
                        
                        {'filei':'../zylow_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zylow_cc-kf_nnlojet_muR2_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zlow_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zylow_cc_muR2_muF2.txt'},
                        
                        {'filei':'../zylow_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zylow_cc-kf_nnlojet_muR05_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zlow_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zylow_cc_muR05_muF1.txt'},
                        
                        {'filei':'../zylow_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zylow_cc-kf_nnlojet_muR1_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zlow_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zylow_cc_muR1_muF05.txt'},
                        
                        {'filei':'../zylow_cc-kf_nnlojet-thexp.dat',
                         'fileo':'./zylow_cc-kf_nnlojet_muR05_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.zlow_cc.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_zylow_cc_muR05_muF05.txt'},
]

opt_map_list_wp = [{'filei':'../wplus-kf_nnlojet-thexp.dat',
                         'fileo':'./wplus-kf_nnlojet_muR1_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wplus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wp_muR1_muF1.txt'},
                        
                        {'filei':'../wplus-kf_nnlojet-thexp.dat',
                         'fileo':'./wplus-kf_nnlojet_muR2_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wplus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wp_muR2_muF1.txt'},
                    
                        {'filei':'../wplus-kf_nnlojet-thexp.dat',
                         'fileo':'./wplus-kf_nnlojet_muR1_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wplus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wp_muR1_muF2.txt'},
                        
                        {'filei':'../wplus-kf_nnlojet-thexp.dat',
                         'fileo':'./wplus-kf_nnlojet_muR2_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wplus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wp_muR2_muF2.txt'},
                        
                        {'filei':'../wplus-kf_nnlojet-thexp.dat',
                         'fileo':'./wplus-kf_nnlojet_muR05_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wplus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wp_muR05_muF1.txt'},
                        
                        {'filei':'../wplus-kf_nnlojet-thexp.dat',
                         'fileo':'./wplus-kf_nnlojet_muR1_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wplus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wp_muR1_muF05.txt'},
                        
                        {'filei':'../wplus-kf_nnlojet-thexp.dat',
                         'fileo':'./wplus-kf_nnlojet_muR05_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wplus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wp_muR05_muF05.txt'},
]

opt_map_list_wm = [{'filei':'../wminus-kf_nnlojet-thexp.dat',
                         'fileo':'./wminus-kf_nnlojet_muR1_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wminus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wm_muR1_muF1.txt'},
                        
                        {'filei':'../wminus-kf_nnlojet-thexp.dat',
                         'fileo':'./wminus-kf_nnlojet_muR2_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wminus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wm_muR2_muF1.txt'},
                    
                        {'filei':'../wminus-kf_nnlojet-thexp.dat',
                         'fileo':'./wminus-kf_nnlojet_muR1_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wminus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wm_muR1_muF2.txt'},
                        
                        {'filei':'../wminus-kf_nnlojet-thexp.dat',
                         'fileo':'./wminus-kf_nnlojet_muR2_muF2-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wminus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wm_muR2_muF2.txt'},
                        
                        {'filei':'../wminus-kf_nnlojet-thexp.dat',
                         'fileo':'./wminus-kf_nnlojet_muR05_muF1-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wminus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wm_muR05_muF1.txt'},
                        
                        {'filei':'../wminus-kf_nnlojet-thexp.dat',
                         'fileo':'./wminus-kf_nnlojet_muR1_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wminus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wm_muR1_muF05.txt'},
                        
                        {'filei':'../wminus-kf_nnlojet-thexp.dat',
                         'fileo':'./wminus-kf_nnlojet_muR05_muF05-thexp.dat',
                         'str':'/kfactor/kf.nnlojet.wminus.txt',
                         'str_sub':'/file_scalevar/kfactor/kf_nnlojet_wm_muR05_muF05.txt'},
]



for opt in opt_map_list_zpeakcc:
    print('copy file ', opt['filei'])
    print('to file ', opt['fileo'])
    copyfile(opt['filei'], opt['fileo']) # copy the file
    #read input file
    fin = open(opt['fileo'], "rt")
    #read file contents to string
    data = fin.read()
    print('replace str ', opt['str'])
    print('with ', opt['str_sub'])
    #replace all occurrences of the required string
    data = data.replace(opt['str'], opt['str_sub'])
    #close the input file
    fin.close()
    #open the input file in write mode
    fin = open(opt['fileo'], "wt")
    #overrite the input file with the resulting data
    fin.write(data)
    #close the file
    fin.close()
    print("")

