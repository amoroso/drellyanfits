!
! The data are taken from arXiv:2008.04174
! xsec from: https://www.hepdata.net/record/ins1810913?version=1&table=Figure%20A1a
! uncertainties from: https://www.hepdata.net/record/ins1810913?version=1&table=Impacts%20Figure%20A11b
! uncertainties are in percent
!
&Data
  Name = 'CMS  WR+ cross section  13 TeV'
  IndexDataset = 5
  Reaction  = 'CC pp'

  TheoryType = 'expression'
  TermName = 'dsigdyW', 'a4yW', 'a0yW'
  TermType = 'reaction'
  TermSource = 'APPLgrid', 'APPLgrid', 'APPLgrid'
  TermInfo = 'GridName=datafiles/lhc/cms/wzProduction/2008.04174/dsig_dyW_Wp_grid.root',
             'GridName=datafiles/lhc/cms/wzProduction/2008.04174/A4_yW_Wp_grid.root',
             'GridName=datafiles/lhc/cms/wzProduction/2008.04174/A0_yW_Wp_grid.root'
  ! fR^Wplus = 1./4. * (2. - a0yW/dsigdyW + a4yW/dsigdyW)
  TheorExpr = '1./4.*(2.-a0yW/dsigdyW+a4yW/dsigdyW)*dsigdyW'

  NDATA = 10
  NColumn = 12
  ColumnType = 'Flag',2*'Bin','Sigma',8*'Error',
  ColumnName = 'binFlag','y1','y2','sigma', 'ignore', 'stat', 'uncor', 'lumi', 'uncor', 'qcd', 'scale', 'pdf'
  Percent   = 13*True
&End
&PlotDesc
    PlotN = 1
    PlotDefColumn = 'y2'
    PlotDefValue = 0., 5.
    PlotOptions(1)  = 'Experiment:CMS@ExtraLabel:pp #rightarrow W^{+}_{R}; #sqrt{s} = 13 TeV; #int L = 35.9 fb^{-1}@XTitle: y_{W} @YTitle: d#sigma/dy_{W} [pb] '
!@YminR:0.61@YmaxR:1.39'
&End
*flag   y1      y2      sigma    tot_unc   datastat   lep_eff   lumi    mc_stat   qcd_bkg   qcd_scales   pdf_alphas
1       0.000   0.250   1365.0   2.480     0.275      0.696     2.110   1.540     0.597     1.220        0.471
1       0.250   0.500   1345.0   2.510     0.284      0.693     2.100   1.590     0.644     1.220        0.492
1       0.500   0.750   1332.0   2.620     0.303      0.738     2.100   1.710     0.718     1.280        0.480
1       0.750   1.000   1250.0   2.770     0.344      0.863     2.090   1.860     0.811     1.330        0.520
1       1.000   1.250   1205.0   2.960     0.386      0.921     2.070   2.060     0.936     1.430        0.555
1       1.250   1.500   1099.0   3.340     0.517      1.100     2.040   2.470     1.080     1.540        0.619
1       1.500   1.750   1084.0   3.870     0.831      1.210     2.050   3.010     1.020     1.660        0.657
1       1.750   2.000   798.0    7.680     2.050      2.360     2.050   6.610     1.900     2.410        1,030
1       2.000   2.250   1210.0   9.400     2.310      3.320     2.260   8.180     2.030     2.560        1.130
1       2.250   2.500   730.0    20.60     3.930      8.220     1.910   17.90     3.810     6.510        1.970
