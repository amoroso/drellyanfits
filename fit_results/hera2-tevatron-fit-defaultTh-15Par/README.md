### Fit HERA1+2 and Tevatron
* Use default xFitter theory.
* Use 15 parameters parametrization, The one used here 1503.05221
* (Use xFitter master branch)

Folder name meaning
* KF - use NNLO kfactor from nlojet for tevatron samples
* fix-as - alpha_s is fixed at 0.118
* q2_10 - use cut Q^2>10 (the others are Q^2>3.5)
* newD0-Zy - use new D0 Zy measurement from 1278533