Minimizer: MINUIT # CERES
MINUIT:
  Commands: |
    call fcn 1
    set str 2
    call fcn 3
    migrad
    hesse
    call fcn 3
    
  doErrors :  Hesse # None

Parameters:
  Ag : DEPENDENT #[ 1.00000000, 0.00000000 ]
  Bg : [0.20402 , 0.02 ]
  Cg : [7.9940, 0.06 ]
  Dg : [ -0.53887, 0.02]
  #Agp : [ 0.07391202, 0.01374756 ]
  #Bgp : [ -0.38249475, 0.01016472 ]
  #Cgp : [ 25.00000000, 0.00000000 ]
  
  Auv : DEPENDENT #[ 1.00000000, 0.00000000 ]
  Buv : [0.68465, 0.007]
  Cuv : [ 4.7148, 0.05 ]
  Duv : [ 0.00000000, 0.00000000 ]
  Euv : [0,0] #[ 9.90725354, 0.81247008 ] 
  Fuv : [3.87, 0.03]
  
  Adv : DEPENDENT #[ 1.00000000, 0.00000000 ]
  Bdv : [ 0.61189, 0.007 ]
  Cdv : [ 7.7521, 0.06 ]
  Ddv : [0,0]
  Edv : [0,0]
  Fdv : [6.34, 0.03]
  
  Aubar : [0,0] #[ 0.16130000, 0.00000000 ]
  Bubar : [0,0] #[ -0.12730000, 0.00000000 ]
  Cubar : [4.64, 0.03]
  Dubar : [-0.17, 0.02]

  Adbar : [0.14631, 0.01]
  Bdbar : [-0.13553, 0.002 ]
  Cdbar: # another example of providing value, step etc.
    value: 13.078
    step: 0.03
    #min
    #max
    #pr_mean
    #pr_sigma
  Ddbar : [17.7, 0.005]
#  As   : [ 0.1075, 0.0    ] # this is not used: use f_s instead
#  Bs   : [  -0.1273, 0.0  ] # this is not used: use f_s instead
#  Cs   : [ 9.586246, 0.0  ] # this is not used: use f_s instead
#  ZERO : [ 0.00000000, 0.00000000 ]
#  fs : [ 0.40000000, 0.00000000 ]
#  alphas : [0.10896, 0.001]

Parameterisations:
  par_g:
    class: HERAPDF
    parameters: [Ag,Bg,Cg,Dg]
  # Another example for Expression parameterisation
  #par_g:
  #  class: Expression
  #  expression: "Ag*(x^Bg*(1-x)^Cg-Agp*x^Bgp*(1-x)^Cgp)"
  par_uv:
    class: Expression
    expression: "Auv * x^Buv * (1-x)^Cuv * e^(Fuv*x)"
    #parameters: [Auv,Buv,Cuv,Duv,Euv]
  par_dv:
    class: Expression
    expression: "Adv * x^Bdv * (1-x)^Cdv * e^(Fdv*x)"
    #parameters: [Adv,Bdv,Cdv]
  par_ubar:
    class: HERAPDF
    parameters: [Adbar,Bdbar,Cubar,Dubar]
  par_dbar:
    class: HERAPDF
    parameters: [Adbar,Bdbar,Cdbar,Ddbar]
  par_s:
    class: HERAPDF
    parameters: [Adbar,Bdbar,Cdbar,Ddbar]
    # parametrise as expression: otherwise how to parametrise it as fs/(1-fs)*par_dbar using HERAPDF?
    #class: Expression
    #expression: "Adbar*fs/(1-fs)*(x^Bdbar*(1-x)^Cdbar)"

DefaultDecomposition: proton
Decompositions:
  proton:
    class: UvDvUbarDbarS
    xuv: par_uv
    xdv: par_dv
    xubar: par_ubar
    xdbar: par_dbar
    xs: par_s
    xg: par_g

#DefaultEvolution: proton-APFELff
DefaultEvolution: proton-QCDNUM
#DefaultEvolution: proton-LHAPDF

Evolutions:
  #proton-APFELff:
    #? !include evolutions/APFEL.yaml
    #decomposition: proton
  proton-QCDNUM:
    ? !include evolutions/QCDNUM.yaml
    decomposition: proton #this could be omitted, as the default decomposition is set
    # The following allows QCDNUM to read PDFs from other evolutions:
    #EvolutionCopy: "proton-LHAPDF"
  proton-LHAPDF:
    class: LHAPDF
    set: "NNPDF30_nlo_as_0118"
    #set: "CT10nlo"
    member: 0
#  proton-APFEL:
#    ? !include evolutions/APFELxx.yaml
#    decomposition: proton
  antiproton:
    class: FlipCharge
    #input: proton-QCDNUM
    input: proton-LHAPDF
#  neutron:
#    class: FlipUD
#    input: proton-QCDNUM

Q0 : 1.378404875209 # Initial scale =sqrt(1.9)

? !include constants.yaml

alphas : 0.118

byReaction:
  # RT DIS scheme settings:
  RT_DISNC:
    ? !include reactions/RT_DISNC.yaml
    # uncomment if defaultEvolution is not QCDNUM: RT_DISNC works with QCDNUM only, use EvolutionCopy
    #evolution: proton-QCDNUM
  # uncomment if defaultEvolution is not QCDNUM: RT_DISNC works with QCDNUM only, use EvolutionCopy
  #BaseDISCC:
  #  evolution: proton-QCDNUM
  # FONLL scheme settings:
  FONLL_DISNC:
    ? !include reactions/FONLL_DISNC.yaml
  FONLL_DISCC:
    ? !include reactions/FONLL_DISCC.yaml
  # FF ABM scheme settings:
  FFABM_DISNC:
    ? !include reactions/FFABM_DISNC.yaml
  FFABM_DISCC:
    ? !include reactions/FFABM_DISCC.yaml
  # AFB settings:
  AFB:
    ? !include reactions/AFB.yaml
  # APPLgrid settings:
  APPLgrid:
    ? !include reactions/APPLgrid.yaml
  # (optional) Fractal module settings:
  Fractal_DISNC:
    ? !include reactions/Fractal_DISNC.yaml

#byDataset: #Here one can redefine some parameters for specific datasets
#  #Parameter definitions here have the highest priority: they override both "byReaction" and "TermInfo"
#  "HERA1+2 NCep 920":
#    epolarity: 2

# Specify HF scheme used for DIS NC processes:
hf_scheme_DISNC :
  defaultValue : 'RT_DISNC'        # global specification
#  defaultValue : 'BaseDISNC'       # global specification
#  defaultValue : 'FONLL_DISNC'     # global specification
#  defaultValue : 'FFABM_DISNC'
#  'HERA1+2 NCep 920' : 'BaseDISNC' # datafile specific (based on name)
#  1 : BaseDISNC
#  'HERA1+2 NCep 920' : 'Fractal_DISNC'  # Fractal model. Add parameters file if you want to try it (see above)

# Specify HF scheme used for DIS CC processes:
hf_scheme_DISCC :
  defaultValue : 'BaseDISCC'       # global specification
#  defaultValue : 'FONLL_DISCC'     # global specification
#  defaultValue : 'FFABM_DISCC'     # global specification

#
# Profiler allows to add variations of parameters and PDF eigenvectors as additional nuisance parameters
#
Profiler:
  Parameters:
    alphas: [ 0.118, 0.119, 0.117 ]  # central, up, (down) variation. If down is not given, uses symmetrizes Up variation 
  #Evolutions:
  #  proton-LHAPDF:
  #    sets:    [CT10]
  #    members: [[0,1,end]]
  Status: "Off"               # "Off" to turn off profiler
  WriteTheo: "Off"            # Can be "Off", "On" or "Asymmetric" (to store asymmetric variations)
  getChi2: "Off"              # determine and report chi2 for each variation

OutputDirectory: "H2-Tev-fix-as_q2-10" #Can be omitted, default is "output"

WriteLHAPDF6:
  name: "proton"
  description: "..."
  authors: "..."
  reference: "..."
    # use DefaultEvolution
  #evolution: proton-QCDNUM
    # take internal grid
  preferInternalGrid:
    # or define grid
  #Xrange: [1e-4, 1]
  #Qrange: [1,1000]
  #Xnpoints: 200
  #Qnpoints: 120
