 First iteration    2934.4717121890208             1094   2.6823324608674777     
After minimisation    2934.47  1094     2.682

  Partial chi2s 
Dataset    1    544.87(+11.01)   317  HERA1+2 NCep 920                                
Dataset    2     67.02( -0.37)    61  HERA1+2 NCep 820                                
Dataset    3    201.00( -2.37)   221  HERA1+2 NCep 575                                
Dataset    4    211.05( -1.96)   177  HERA1+2 NCep 460                                
Dataset    5    345.70( +7.76)   159  HERA1+2 NCem                                    
Dataset    6     50.54( +0.46)    39  HERA1+2 CCep                                    
Dataset    7     99.02( +2.45)    42  HERA1+2 CCem                                    
Dataset    8    128.15( +1.33)    28  D0 Z rapidity 2007                              
Dataset    9    442.18( +0.00)    10  D0 W->mu nu lepton asymmetry ptl > 25 GeV       
Dataset   10    325.05( -0.00)    14  D0 W asymmetry 2013                             
Dataset   11     28.44( -1.85)    28  CDF Z rapidity 2010                             
Dataset   12     94.83( -0.00)    13  CDF W asymmetry 2009                            

 Correlated Chi2    380.17913959992819     
 Log penalty Chi2    16.445091869348833     
 Systematic shifts          212
  
                                Name          Shift   +/-    Error          Type
    1  sysHZComb1001                          0.4204   +/-   0.9327        :N:M:D
    2  sysHZComb1002                         -0.9100   +/-   0.9370        :N:M:D
    3  sysHZComb1003                          2.6032   +/-   0.5607        :N:M:D
    4  sysHZComb1004                          1.6894   +/-   0.6239        :N:M:D
    5  sysHZComb1005                          0.1889   +/-   0.6147        :N:M:D
    6  sysHZComb1006                         -0.0471   +/-   0.7959        :N:M:D
    7  sysHZComb1007                          0.0329   +/-   0.9997        :N:M:D
    8  sysHZComb1008                          0.0484   +/-   0.9998        :N:M:D
    9  sysHZComb1009                          0.1385   +/-   0.9013        :N:M:D
   10  sysHZComb1010                         -0.1048   +/-   0.9974        :N:M:D
   11  sysHZComb1011                         -0.3305   +/-   0.9840        :N:M:D
   12  sysHZComb1012                          0.3027   +/-   0.9888        :N:M:D
   13  sysHZComb1013                         -0.4254   +/-   0.9879        :N:M:D
   14  sysHZComb1014                          0.0423   +/-   0.9990        :N:M:D
   15  sysHZComb1015                          0.1803   +/-   0.9942        :N:M:D
   16  sysHZComb1016                          0.2513   +/-   0.9774        :N:M:D
   17  sysHZComb1017                         -0.1544   +/-   0.9907        :N:M:D
   18  sysHZComb1018                          0.6934   +/-   0.8260        :N:M:D
   19  sysHZComb1019                          0.1663   +/-   0.9915        :N:M:D
   20  sysHZComb1020                         -0.1960   +/-   0.9954        :N:M:D
   21  sysHZComb1021                         -0.0777   +/-   0.9932        :N:M:D
   22  sysHZComb1022                         -0.0915   +/-   0.9876        :N:M:D
   23  sysHZComb1023                         -0.1436   +/-   0.9515        :N:M:D
   24  sysHZComb1024                          0.1030   +/-   0.9884        :N:M:D
   25  sysHZComb1025                          0.0022   +/-   0.9965        :N:M:D
   26  sysHZComb1026                         -0.8370   +/-   0.9651        :N:M:D
   27  sysHZComb1027                         -0.3092   +/-   0.9734        :N:M:D
   28  sysHZComb1028                          0.0323   +/-   0.9891        :N:M:D
   29  sysHZComb1029                         -1.1665   +/-   0.9385        :N:M:D
   30  sysHZComb1030                          0.3856   +/-   0.9827        :N:M:D
   31  sysHZComb1031                         -0.1955   +/-   0.8755        :N:M:D
   32  sysHZComb1032                          0.0655   +/-   0.9842        :N:M:D
   33  sysHZComb1033                          0.3191   +/-   0.9781        :N:M:D
   34  sysHZComb1034                          0.1456   +/-   0.9887        :N:M:D
   35  sysHZComb1035                         -0.5533   +/-   0.9809        :N:M:D
   36  sysHZComb1036                         -0.0445   +/-   0.9908        :N:M:D
   37  sysHZComb1037                          0.2068   +/-   0.9878        :N:M:D
   38  sysHZComb1038                         -0.0813   +/-   0.9265        :N:M:D
   39  sysHZComb1039                          0.0646   +/-   0.9674        :N:M:D
   40  sysHZComb1040                         -0.8829   +/-   0.9464        :N:M:D
   41  sysHZComb1041                         -0.5518   +/-   0.8338        :N:M:D
   42  sysHZComb1042                          0.1341   +/-   0.8880        :N:M:D
   43  sysHZComb1043                         -1.9543   +/-   0.9268        :N:M:D
   44  sysHZComb1044                         -0.2088   +/-   0.9568        :N:M:D
   45  sysHZComb1045                          0.8877   +/-   0.9420        :N:M:D
   46  sysHZComb1046                          1.3170   +/-   0.9658        :N:M:D
   47  sysHZComb1047                         -0.3650   +/-   0.9526        :N:M:D
   48  sysHZComb1048                         -0.2640   +/-   0.9826        :N:M:D
   49  sysHZComb1049                         -0.9615   +/-   0.9489        :N:M:D
   50  sysHZComb1050                          0.6197   +/-   0.9794        :N:M:D
   51  sysHZComb1051                          1.1339   +/-   0.9744        :N:M:D
   52  sysHZComb1052                         -0.4331   +/-   0.9671        :N:M:D
   53  sysHZComb1053                         -2.1147   +/-   0.9539        :N:M:D
   54  sysHZComb1054                         -0.3976   +/-   0.9893        :N:M:D
   55  sysHZComb1055                          0.2829   +/-   0.9466        :N:M:D
   56  sysHZComb1056                         -2.5580   +/-   0.9089        :N:M:D
   57  sysHZComb1057                          0.5545   +/-   0.9233        :N:M:D
   58  sysHZComb1058                          0.8466   +/-   0.9741        :N:M:D
   59  sysHZComb1059                         -0.6641   +/-   0.8922        :N:M:D
   60  sysHZComb1060                          0.4460   +/-   0.9793        :N:M:D
   61  sysHZComb1061                         -0.2151   +/-   0.9921        :N:M:D
   62  sysHZComb1062                         -0.8860   +/-   0.8970        :N:M:D
   63  sysHZComb1063                          0.5977   +/-   0.8693        :N:M:D
   64  sysHZComb1064                          0.7514   +/-   0.9712        :N:M:D
   65  sysHZComb1065                         -1.2886   +/-   0.9541        :N:M:D
   66  sysHZComb1066                         -1.2088   +/-   0.9477        :N:M:D
   67  sysHZComb1067                         -1.2604   +/-   0.9793        :N:M:D
   68  sysHZComb1068                         -1.1051   +/-   0.9731        :N:M:D
   69  sysHZComb1069                          1.8995   +/-   0.9286        :N:M:D
   70  sysHZComb1070                          1.0003   +/-   0.9161        :N:M:D
   71  sysHZComb1071                         -1.9018   +/-   0.8721        :N:M:D
   72  sysHZComb1072                         -0.6140   +/-   0.9718        :N:M:D
   73  sysHZComb1073                         -0.3348   +/-   0.9491        :N:M:D
   74  sysHZComb1074                         -1.2240   +/-   0.9738        :N:M:D
   75  sysHZComb1075                         -0.1586   +/-   0.8747        :N:M:D
   76  sysHZComb1076                          0.5904   +/-   0.9886        :N:M:D
   77  sysHZComb1077                         -0.9600   +/-   0.9623        :N:M:D
   78  sysHZComb1078                         -0.5157   +/-   0.9934        :N:M:D
   79  sysHZComb1079                         -1.3150   +/-   0.9645        :N:M:D
   80  sysHZComb1080                         -1.1719   +/-   0.9270        :N:M:D
   81  sysHZComb1081                         -1.4115   +/-   0.8894        :N:M:D
   82  sysHZComb1082                         -1.2805   +/-   0.8552        :N:M:D
   83  sysHZComb1083                         -0.8272   +/-   0.9003        :N:M:D
   84  sysHZComb1084                          0.2834   +/-   0.9689        :N:M:D
   85  sysHZComb1085                          0.2186   +/-   0.8900        :N:M:D
   86  sysHZComb1086                          2.0786   +/-   0.8070        :N:M:D
   87  sysHZComb1087                         -0.8543   +/-   0.9304        :N:M:D
   88  sysHZComb1088                         -0.0155   +/-   0.9986        :N:M:D
   89  sysHZComb1089                         -0.2469   +/-   0.9683        :N:M:D
   90  sysHZComb1090                          0.4595   +/-   0.9480        :N:M:D
   91  sysHZComb1091                          0.5631   +/-   0.8907        :N:M:D
   92  sysHZComb1092                         -0.1081   +/-   0.8243        :N:M:D
   93  sysHZComb1093                          0.2125   +/-   0.8122        :N:M:D
   94  sysHZComb1094                         -0.0361   +/-   0.9998        :N:M:D
   95  sysHZComb1095                         -1.7818   +/-   0.8443        :N:M:D
   96  sysHZComb1096                         -0.0011   +/-   0.8965        :N:M:D
   97  sysHZComb1097                          0.0881   +/-   0.9883        :N:M:D
   98  sysHZComb1098                          1.4161   +/-   0.7802        :N:M:D
   99  sysHZComb1099                         -0.7195   +/-   0.7578        :N:M:D
  100  sysHZComb1100                         -0.8932   +/-   0.7184        :N:M:D
  101  sysHZComb1101                          1.2087   +/-   0.7834        :N:M:D
  102  sysHZComb1102                         -1.2937   +/-   0.9053        :N:M:D
  103  sysHZComb1103                          1.0354   +/-   0.8100        :N:M:D
  104  sysHZComb1104                         -0.9868   +/-   0.9014        :N:M:D
  105  sysHZComb1105                          0.8832   +/-   0.7954        :N:M:D
  106  sysHZComb1106                         -0.7705   +/-   0.7940        :N:M:D
  107  sysHZComb1107                          2.2649   +/-   0.7501        :N:M:D
  108  sysHZComb1108                          2.1019   +/-   0.6963        :N:M:D
  109  sysHZComb1109                          0.5555   +/-   0.9919        :N:M:D
  110  sysHZComb1110                         -1.7885   +/-   0.7382        :N:M:D
  111  sysHZComb1111                         -1.8282   +/-   0.5622        :N:M:D
  112  sysHZComb1112                         -0.2194   +/-   0.9918        :N:M:D
  113  sysHZComb1113                          1.9224   +/-   0.8380        :N:M:D
  114  sysHZComb1114                         -0.4773   +/-   0.8427        :N:M:D
  115  sysHZComb1115                         -1.4366   +/-   0.8355        :N:M:D
  116  sysHZComb1116                         -0.3223   +/-   0.8391        :N:M:D
  117  sysHZComb1117                          0.2783   +/-   0.8195        :N:M:D
  118  sysHZComb1118                          0.0342   +/-   0.9982        :N:M:D
  119  sysHZComb1119                         -0.1699   +/-   0.9905        :N:M:D
  120  sysHZComb1120                         -0.4278   +/-   0.8518        :N:M:D
  121  sysHZComb1121                          0.0344   +/-   0.9957        :N:M:D
  122  sysHZComb1122                          0.4982   +/-   0.9320        :N:M:D
  123  sysHZComb1123                         -0.1700   +/-   0.9295        :N:M:D
  124  sysHZComb1124                          0.9868   +/-   0.8709        :N:M:D
  125  sysHZComb1125                          0.0437   +/-   0.8651        :N:M:D
  126  sysHZComb1126                         -0.0443   +/-   0.9408        :N:M:D
  127  sysHZComb1127                          1.2458   +/-   0.9330        :N:M:D
  128  sysHZComb1128                         -0.3107   +/-   0.9608        :N:M:D
  129  sysHZComb1129                          1.9349   +/-   0.8625        :N:M:D
  130  sysHZComb1130                         -0.1626   +/-   0.8213        :N:M:D
  131  sysHZComb1131                         -3.4748   +/-   0.8202        :N:M:D
  132  sysHZComb1132                          4.3348   +/-   0.7978        :N:M:D
  133  sysHZComb1133                         -2.2317   +/-   0.8395        :N:M:D
  134  sysHZComb1134                          0.0987   +/-   0.9824        :N:M:D
  135  sysHZComb1135                          1.5295   +/-   0.8389        :N:M:D
  136  sysHZComb1136                          1.4210   +/-   0.8929        :N:M:D
  137  sysHZComb1137                         -0.5989   +/-   0.9169        :N:M:D
  138  sysHZComb1138                          0.9049   +/-   0.9232        :N:M:D
  139  sysHZComb1139                         -0.3136   +/-   0.9578        :N:M:D
  140  sysHZComb1140                          0.0143   +/-   0.9615        :N:M:D
  141  sysHZComb1141                          0.0681   +/-   0.9904        :N:M:D
  142  sysHZComb1142                         -1.5010   +/-   0.8627        :N:M:D
  143  sysHZComb1143                         -2.3532   +/-   0.8560        :N:M:D
  144  sysHZComb1144                          0.2440   +/-   0.8465        :N:M:D
  145  sysHZComb1145                         -0.0963   +/-   0.9967        :N:M:D
  146  sysHZComb1146                         -0.1968   +/-   0.9959        :N:M:D
  147  sysHZComb1147                          1.3110   +/-   0.8155        :N:M:D
  148  sysHZComb1148                         -1.1593   +/-   0.8581        :N:M:D
  149  sysHZComb1149                          0.8905   +/-   0.7592        :N:M:D
  150  sysHZComb1150                         -1.4585   +/-   0.7901        :N:M:D
  151  sysHZComb1151                         -0.1513   +/-   0.9960        :N:M:D
  152  sysHZComb1152                          0.7732   +/-   0.7352        :N:M:D
  153  sysHZComb1153                         -2.2185   +/-   0.8387        :N:M:D
  154  sysHZComb1154                          1.4107   +/-   0.8342        :N:M:D
  155  sysHZComb1155                          1.1752   +/-   0.8407        :N:M:D
  156  sysHZComb1156                         -0.8356   +/-   0.9165        :N:M:D
  157  sysHZComb1157                          0.3793   +/-   0.7523        :N:M:D
  158  sysHZComb1158                          0.2317   +/-   0.8136        :N:M:D
  159  sysHZComb1159                         -0.0014   +/-   1.0000        :N:M:D
  160  sysHZComb1160                         -0.2508   +/-   0.8895        :N:M:D
  161  sysHZComb1161                          0.5494   +/-   0.8635        :N:M:D
  162  sysHZComb1162                          3.1460   +/-   0.8895        :N:M:D
  163  proc_nrl                               1.3175   +/-   0.4088        :N:M:D
  164  proc_tb21                             -0.8237   +/-   0.9111        :N:M:D
  165  proc_tb22                             -1.4449   +/-   0.3501        :N:M:D
  166  proc_tb23                             -0.5594   +/-   0.4325        :N:M:D
  167  proc_tb24                              1.1142   +/-   0.4134        :N:M:D
  168  proc_gp                                0.8816   +/-   0.6189        :N:M:D
  169  proc_had                              -0.3869   +/-   0.6026        :N:M:D
  170  e_Escale                               0.2428   +/-   0.4160        :N:M:D
  171  D0_WMUA_EWbkg                          3.8758   +/-   0.9649        :N:A:D
  172  D0_WMUA_MJbkg                          1.8197   +/-   0.9291        :N:A:D
  173  D0_WMUA_charge_misid                   1.0249   +/-   0.9967        :N:A:D
  174  D0_WMUA_rel_charge_efficiency          5.5106   +/-   0.9593        :N:A:D
  175  D0_WMUA_magnet_pol_weighting           0.2689   +/-   0.9981        :N:A:D
  176  D0_WMUA_momentum_met_resolution        7.8291   +/-   0.5279        :N:A:D
  177  D0_WA_pdf_01                           1.6414   +/-   0.9876        :N:A:D
  178  D0_WA_pdf_02                          -1.0626   +/-   0.9972        :N:A:D
  179  D0_WA_pdf_03                           0.1137   +/-   0.9997        :N:A:D
  180  D0_WA_pdf_04                           1.3056   +/-   0.9882        :N:A:D
  181  D0_WA_pdf_05                           1.2936   +/-   0.9855        :N:A:D
  182  D0_WA_pdf_06                          -1.3094   +/-   0.9938        :N:A:D
  183  D0_WA_pdf_07                          -0.7073   +/-   0.9938        :N:A:D
  184  D0_WA_pdf_08                           0.0757   +/-   0.9995        :N:A:D
  185  D0_WA_pdf_09                          -1.7550   +/-   0.9432        :N:A:D
  186  D0_WA_pdf_10                          -3.8381   +/-   0.9694        :N:A:D
  187  D0_WA_pdf_11                          -0.6273   +/-   0.9852        :N:A:D
  188  D0_WA_pdf_12                           0.6629   +/-   0.9946        :N:A:D
  189  D0_WA_pdf_13                           0.6699   +/-   0.9978        :N:A:D
  190  D0_WA_pdf_14                           1.7433   +/-   0.9866        :N:A:D
  191  D0_WA_pdf_15                           2.3308   +/-   0.9768        :N:A:D
  192  D0_WA_pdf_16                          -0.7909   +/-   0.9956        :N:A:D
  193  D0_WA_pdf_17                           1.1559   +/-   0.8899        :N:A:D
  194  D0_WA_pdf_18                          -0.7136   +/-   0.9774        :N:A:D
  195  D0_WA_pdf_19                          -0.4388   +/-   0.9957        :N:A:D
  196  D0_WA_pdf_20                           0.9599   +/-   0.9941        :N:A:D
  197  D0_WA_pdf_21                          -0.2831   +/-   0.9995        :N:A:D
  198  D0_WA_pdf_22                           0.0000   +/-   1.0000        :N:A:D
  199  CDF_ZY_lum                             1.2921   +/-   0.0941        :N:M:D
  200  CDF_ZY_background_CC                   0.0350   +/-   0.9993        :N:A:D
  201  CDF_ZY_background_CF                  -0.1701   +/-   0.9156        :N:A:D
  202  CDF_ZY_background_FF                  -0.9307   +/-   0.7723        :N:A:D
  203  CDF_ZY_CMaterial                      -0.3472   +/-   0.9615        :N:M:D
  204  CDF_ZY_FMaterial                      -0.2203   +/-   0.9861        :N:M:D
  205  CDF_ZY_ZVtx                            0.3975   +/-   0.9263        :N:M:D
  206  CDF_ZY_Trkeff                         -0.1659   +/-   0.9510        :N:M:D
  207  CDF_ZY_NoTrk                           0.2400   +/-   0.9743        :N:M:D
  208  CDF_WA_chmisid                         2.5754   +/-   0.9296        :N:A:D
  209  CDF_WA_background                     -1.1808   +/-   0.9303        :N:A:D
  210  CDF_WA_energyscale                     4.0506   +/-   0.8851        :N:A:D
  211  CDF_WA_recoil                          1.6782   +/-   0.5476        :N:A:D
  212  CDF_WA_pdf                             1.5467   +/-   0.8797        :N:A:D
After minimisation    1312.46  1094     1.200

  Partial chi2s 
Dataset    1    346.64( +7.03)   317  HERA1+2 NCep 920                                
Dataset    2     54.49( +0.45)    61  HERA1+2 NCep 820                                
Dataset    3    187.74( +0.35)   221  HERA1+2 NCep 575                                
Dataset    4    198.58( -0.79)   177  HERA1+2 NCep 460                                
Dataset    5    223.06( +0.53)   159  HERA1+2 NCem                                    
Dataset    6     45.72( +1.34)    39  HERA1+2 CCep                                    
Dataset    7     67.68( -1.30)    42  HERA1+2 CCem                                    
Dataset    8     38.74( +0.33)    28  D0 Z rapidity 2007                              
Dataset    9     11.73( +0.00)    10  D0 W->mu nu lepton asymmetry ptl > 25 GeV       
Dataset   10     14.59( -0.00)    14  D0 W asymmetry 2013                             
Dataset   11     28.44( -1.85)    28  CDF Z rapidity 2010                             
Dataset   12     19.85( -0.00)    13  CDF W asymmetry 2009                            

 Correlated Chi2    69.121286562391688     
 Log penalty Chi2    6.0789910798226403     
 Systematic shifts          212
  
                                Name          Shift   +/-    Error          Type
    1  sysHZComb1001                          0.3908   +/-   0.9327        :N:M:D
    2  sysHZComb1002                         -0.5362   +/-   0.9369        :N:M:D
    3  sysHZComb1003                          1.2839   +/-   0.5599        :N:M:D
    4  sysHZComb1004                          0.0535   +/-   0.6237        :N:M:D
    5  sysHZComb1005                          1.4046   +/-   0.6146        :N:M:D
    6  sysHZComb1006                         -1.3422   +/-   0.7958        :N:M:D
    7  sysHZComb1007                          0.0151   +/-   0.9997        :N:M:D
    8  sysHZComb1008                          0.0043   +/-   0.9998        :N:M:D
    9  sysHZComb1009                          0.1150   +/-   0.8996        :N:M:D
   10  sysHZComb1010                         -0.0359   +/-   0.9974        :N:M:D
   11  sysHZComb1011                         -0.5267   +/-   0.9840        :N:M:D
   12  sysHZComb1012                          0.0176   +/-   0.9888        :N:M:D
   13  sysHZComb1013                         -0.3523   +/-   0.9879        :N:M:D
   14  sysHZComb1014                         -0.0650   +/-   0.9990        :N:M:D
   15  sysHZComb1015                          0.1844   +/-   0.9942        :N:M:D
   16  sysHZComb1016                          0.0862   +/-   0.9773        :N:M:D
   17  sysHZComb1017                          0.0742   +/-   0.9907        :N:M:D
   18  sysHZComb1018                          0.3119   +/-   0.8249        :N:M:D
   19  sysHZComb1019                         -0.0419   +/-   0.9915        :N:M:D
   20  sysHZComb1020                          0.0778   +/-   0.9954        :N:M:D
   21  sysHZComb1021                          0.0396   +/-   0.9932        :N:M:D
   22  sysHZComb1022                          0.0587   +/-   0.9876        :N:M:D
   23  sysHZComb1023                         -0.2860   +/-   0.9513        :N:M:D
   24  sysHZComb1024                         -0.0968   +/-   0.9884        :N:M:D
   25  sysHZComb1025                          0.0841   +/-   0.9965        :N:M:D
   26  sysHZComb1026                         -0.4823   +/-   0.9649        :N:M:D
   27  sysHZComb1027                         -0.6809   +/-   0.9733        :N:M:D
   28  sysHZComb1028                          0.3425   +/-   0.9891        :N:M:D
   29  sysHZComb1029                         -0.0881   +/-   0.9382        :N:M:D
   30  sysHZComb1030                         -0.1133   +/-   0.9826        :N:M:D
   31  sysHZComb1031                         -0.1876   +/-   0.8748        :N:M:D
   32  sysHZComb1032                         -0.1825   +/-   0.9841        :N:M:D
   33  sysHZComb1033                          0.4467   +/-   0.9779        :N:M:D
   34  sysHZComb1034                         -0.2343   +/-   0.9886        :N:M:D
   35  sysHZComb1035                          0.2378   +/-   0.9810        :N:M:D
   36  sysHZComb1036                         -0.0068   +/-   0.9907        :N:M:D
   37  sysHZComb1037                          0.1243   +/-   0.9877        :N:M:D
   38  sysHZComb1038                          0.3022   +/-   0.9262        :N:M:D
   39  sysHZComb1039                          0.0889   +/-   0.9672        :N:M:D
   40  sysHZComb1040                         -0.0612   +/-   0.9462        :N:M:D
   41  sysHZComb1041                         -0.4529   +/-   0.8322        :N:M:D
   42  sysHZComb1042                         -0.1643   +/-   0.8872        :N:M:D
   43  sysHZComb1043                         -0.5036   +/-   0.9266        :N:M:D
   44  sysHZComb1044                         -0.3263   +/-   0.9567        :N:M:D
   45  sysHZComb1045                         -0.4872   +/-   0.9419        :N:M:D
   46  sysHZComb1046                          0.0310   +/-   0.9658        :N:M:D
   47  sysHZComb1047                          0.0823   +/-   0.9529        :N:M:D
   48  sysHZComb1048                         -0.2950   +/-   0.9827        :N:M:D
   49  sysHZComb1049                         -0.6284   +/-   0.9491        :N:M:D
   50  sysHZComb1050                          0.4609   +/-   0.9794        :N:M:D
   51  sysHZComb1051                          0.5158   +/-   0.9743        :N:M:D
   52  sysHZComb1052                          0.3373   +/-   0.9671        :N:M:D
   53  sysHZComb1053                         -1.8472   +/-   0.9536        :N:M:D
   54  sysHZComb1054                         -0.2416   +/-   0.9893        :N:M:D
   55  sysHZComb1055                         -0.5866   +/-   0.9465        :N:M:D
   56  sysHZComb1056                         -0.8383   +/-   0.9093        :N:M:D
   57  sysHZComb1057                          0.2364   +/-   0.9240        :N:M:D
   58  sysHZComb1058                          0.5818   +/-   0.9741        :N:M:D
   59  sysHZComb1059                         -0.2754   +/-   0.8918        :N:M:D
   60  sysHZComb1060                          0.0257   +/-   0.9795        :N:M:D
   61  sysHZComb1061                         -0.2498   +/-   0.9921        :N:M:D
   62  sysHZComb1062                         -1.7337   +/-   0.8981        :N:M:D
   63  sysHZComb1063                          0.1267   +/-   0.8691        :N:M:D
   64  sysHZComb1064                          0.1508   +/-   0.9713        :N:M:D
   65  sysHZComb1065                         -0.6866   +/-   0.9541        :N:M:D
   66  sysHZComb1066                         -0.4583   +/-   0.9476        :N:M:D
   67  sysHZComb1067                         -0.3139   +/-   0.9793        :N:M:D
   68  sysHZComb1068                          0.0404   +/-   0.9732        :N:M:D
   69  sysHZComb1069                          0.4160   +/-   0.9288        :N:M:D
   70  sysHZComb1070                          0.0551   +/-   0.9162        :N:M:D
   71  sysHZComb1071                         -1.1718   +/-   0.8714        :N:M:D
   72  sysHZComb1072                         -0.0880   +/-   0.9722        :N:M:D
   73  sysHZComb1073                          0.0202   +/-   0.9490        :N:M:D
   74  sysHZComb1074                         -0.6579   +/-   0.9737        :N:M:D
   75  sysHZComb1075                         -0.3352   +/-   0.8749        :N:M:D
   76  sysHZComb1076                          0.3808   +/-   0.9886        :N:M:D
   77  sysHZComb1077                         -0.4868   +/-   0.9623        :N:M:D
   78  sysHZComb1078                         -0.1632   +/-   0.9934        :N:M:D
   79  sysHZComb1079                         -0.6779   +/-   0.9643        :N:M:D
   80  sysHZComb1080                         -0.6207   +/-   0.9275        :N:M:D
   81  sysHZComb1081                         -0.2610   +/-   0.8898        :N:M:D
   82  sysHZComb1082                         -0.4653   +/-   0.8552        :N:M:D
   83  sysHZComb1083                         -0.9474   +/-   0.9002        :N:M:D
   84  sysHZComb1084                          0.3229   +/-   0.9688        :N:M:D
   85  sysHZComb1085                          0.2129   +/-   0.8899        :N:M:D
   86  sysHZComb1086                          0.4455   +/-   0.8071        :N:M:D
   87  sysHZComb1087                          0.0541   +/-   0.9302        :N:M:D
   88  sysHZComb1088                         -0.0127   +/-   0.9986        :N:M:D
   89  sysHZComb1089                          0.0530   +/-   0.9685        :N:M:D
   90  sysHZComb1090                          0.8237   +/-   0.9485        :N:M:D
   91  sysHZComb1091                          0.8775   +/-   0.8909        :N:M:D
   92  sysHZComb1092                          0.1297   +/-   0.8234        :N:M:D
   93  sysHZComb1093                          0.1186   +/-   0.8115        :N:M:D
   94  sysHZComb1094                         -0.0203   +/-   0.9998        :N:M:D
   95  sysHZComb1095                         -0.2931   +/-   0.8463        :N:M:D
   96  sysHZComb1096                          0.0367   +/-   0.8972        :N:M:D
   97  sysHZComb1097                          0.2244   +/-   0.9883        :N:M:D
   98  sysHZComb1098                          1.5680   +/-   0.7840        :N:M:D
   99  sysHZComb1099                         -1.1103   +/-   0.7598        :N:M:D
  100  sysHZComb1100                         -0.4553   +/-   0.7183        :N:M:D
  101  sysHZComb1101                         -0.2624   +/-   0.7851        :N:M:D
  102  sysHZComb1102                         -0.4586   +/-   0.9063        :N:M:D
  103  sysHZComb1103                          0.2452   +/-   0.8106        :N:M:D
  104  sysHZComb1104                         -0.4615   +/-   0.9008        :N:M:D
  105  sysHZComb1105                          1.1928   +/-   0.7944        :N:M:D
  106  sysHZComb1106                         -0.6646   +/-   0.7935        :N:M:D
  107  sysHZComb1107                          0.8085   +/-   0.7499        :N:M:D
  108  sysHZComb1108                          1.9281   +/-   0.6951        :N:M:D
  109  sysHZComb1109                          0.3961   +/-   0.9919        :N:M:D
  110  sysHZComb1110                         -0.9132   +/-   0.7372        :N:M:D
  111  sysHZComb1111                         -1.7905   +/-   0.5609        :N:M:D
  112  sysHZComb1112                          0.0694   +/-   0.9918        :N:M:D
  113  sysHZComb1113                          0.9713   +/-   0.8408        :N:M:D
  114  sysHZComb1114                          0.9719   +/-   0.8425        :N:M:D
  115  sysHZComb1115                         -0.6339   +/-   0.8347        :N:M:D
  116  sysHZComb1116                         -0.1128   +/-   0.8387        :N:M:D
  117  sysHZComb1117                          1.0702   +/-   0.8199        :N:M:D
  118  sysHZComb1118                          0.0231   +/-   0.9982        :N:M:D
  119  sysHZComb1119                         -0.0850   +/-   0.9905        :N:M:D
  120  sysHZComb1120                          0.9900   +/-   0.8540        :N:M:D
  121  sysHZComb1121                         -0.0644   +/-   0.9957        :N:M:D
  122  sysHZComb1122                          0.1598   +/-   0.9332        :N:M:D
  123  sysHZComb1123                         -0.3163   +/-   0.9297        :N:M:D
  124  sysHZComb1124                         -1.2743   +/-   0.8716        :N:M:D
  125  sysHZComb1125                          0.9110   +/-   0.8732        :N:M:D
  126  sysHZComb1126                         -0.1781   +/-   0.9405        :N:M:D
  127  sysHZComb1127                          0.1099   +/-   0.9331        :N:M:D
  128  sysHZComb1128                         -0.3000   +/-   0.9608        :N:M:D
  129  sysHZComb1129                          0.0561   +/-   0.8633        :N:M:D
  130  sysHZComb1130                          0.1797   +/-   0.8213        :N:M:D
  131  sysHZComb1131                         -0.1625   +/-   0.8217        :N:M:D
  132  sysHZComb1132                         -0.5224   +/-   0.7988        :N:M:D
  133  sysHZComb1133                          0.3515   +/-   0.8396        :N:M:D
  134  sysHZComb1134                          0.3583   +/-   0.9824        :N:M:D
  135  sysHZComb1135                         -1.1248   +/-   0.8398        :N:M:D
  136  sysHZComb1136                          0.4402   +/-   0.8932        :N:M:D
  137  sysHZComb1137                         -0.1838   +/-   0.9171        :N:M:D
  138  sysHZComb1138                          0.3724   +/-   0.9231        :N:M:D
  139  sysHZComb1139                         -0.7214   +/-   0.9576        :N:M:D
  140  sysHZComb1140                         -0.1854   +/-   0.9616        :N:M:D
  141  sysHZComb1141                         -0.0860   +/-   0.9904        :N:M:D
  142  sysHZComb1142                          1.1395   +/-   0.8644        :N:M:D
  143  sysHZComb1143                          0.2193   +/-   0.8577        :N:M:D
  144  sysHZComb1144                          1.4192   +/-   0.8465        :N:M:D
  145  sysHZComb1145                         -0.1237   +/-   0.9967        :N:M:D
  146  sysHZComb1146                         -0.1099   +/-   0.9959        :N:M:D
  147  sysHZComb1147                          0.5569   +/-   0.8153        :N:M:D
  148  sysHZComb1148                         -0.5567   +/-   0.8578        :N:M:D
  149  sysHZComb1149                          0.5504   +/-   0.7696        :N:M:D
  150  sysHZComb1150                         -0.7096   +/-   0.7903        :N:M:D
  151  sysHZComb1151                         -0.1319   +/-   0.9960        :N:M:D
  152  sysHZComb1152                          0.5961   +/-   0.7373        :N:M:D
  153  sysHZComb1153                         -1.4781   +/-   0.8386        :N:M:D
  154  sysHZComb1154                          0.0113   +/-   0.8342        :N:M:D
  155  sysHZComb1155                          1.1019   +/-   0.8406        :N:M:D
  156  sysHZComb1156                         -0.0880   +/-   0.9165        :N:M:D
  157  sysHZComb1157                          0.5678   +/-   0.7521        :N:M:D
  158  sysHZComb1158                         -0.4766   +/-   0.8127        :N:M:D
  159  sysHZComb1159                         -0.0020   +/-   1.0000        :N:M:D
  160  sysHZComb1160                         -0.6747   +/-   0.8890        :N:M:D
  161  sysHZComb1161                          0.4090   +/-   0.8626        :N:M:D
  162  sysHZComb1162                          0.9248   +/-   0.8893        :N:M:D
  163  proc_nrl                               0.1919   +/-   0.4093        :N:M:D
  164  proc_tb21                             -0.6498   +/-   0.9110        :N:M:D
  165  proc_tb22                             -1.2369   +/-   0.3492        :N:M:D
  166  proc_tb23                             -0.1817   +/-   0.4340        :N:M:D
  167  proc_tb24                             -0.9555   +/-   0.4125        :N:M:D
  168  proc_gp                               -0.5526   +/-   0.6183        :N:M:D
  169  proc_had                              -0.2520   +/-   0.6022        :N:M:D
  170  e_Escale                               0.0066   +/-   0.4176        :N:M:D
  171  D0_WMUA_EWbkg                         -0.0685   +/-   0.9649        :N:A:D
  172  D0_WMUA_MJbkg                          0.3542   +/-   0.9291        :N:A:D
  173  D0_WMUA_charge_misid                   0.0201   +/-   0.9967        :N:A:D
  174  D0_WMUA_rel_charge_efficiency         -0.1151   +/-   0.9593        :N:A:D
  175  D0_WMUA_magnet_pol_weighting          -0.0136   +/-   0.9981        :N:A:D
  176  D0_WMUA_momentum_met_resolution       -0.1565   +/-   0.5279        :N:A:D
  177  D0_WA_pdf_01                           0.0957   +/-   0.9876        :N:A:D
  178  D0_WA_pdf_02                           0.0026   +/-   0.9972        :N:A:D
  179  D0_WA_pdf_03                           0.0041   +/-   0.9997        :N:A:D
  180  D0_WA_pdf_04                           0.0853   +/-   0.9882        :N:A:D
  181  D0_WA_pdf_05                           0.0715   +/-   0.9855        :N:A:D
  182  D0_WA_pdf_06                          -0.0740   +/-   0.9938        :N:A:D
  183  D0_WA_pdf_07                          -0.0595   +/-   0.9938        :N:A:D
  184  D0_WA_pdf_08                           0.0083   +/-   0.9995        :N:A:D
  185  D0_WA_pdf_09                          -0.3232   +/-   0.9432        :N:A:D
  186  D0_WA_pdf_10                           0.0068   +/-   0.9694        :N:A:D
  187  D0_WA_pdf_11                           0.1011   +/-   0.9852        :N:A:D
  188  D0_WA_pdf_12                           0.0623   +/-   0.9946        :N:A:D
  189  D0_WA_pdf_13                          -0.0340   +/-   0.9978        :N:A:D
  190  D0_WA_pdf_14                           0.1291   +/-   0.9866        :N:A:D
  191  D0_WA_pdf_15                           0.1654   +/-   0.9768        :N:A:D
  192  D0_WA_pdf_16                          -0.0986   +/-   0.9956        :N:A:D
  193  D0_WA_pdf_17                           0.3329   +/-   0.8899        :N:A:D
  194  D0_WA_pdf_18                          -0.2255   +/-   0.9774        :N:A:D
  195  D0_WA_pdf_19                          -0.0094   +/-   0.9957        :N:A:D
  196  D0_WA_pdf_20                           0.0519   +/-   0.9941        :N:A:D
  197  D0_WA_pdf_21                          -0.0578   +/-   0.9995        :N:A:D
  198  D0_WA_pdf_22                           0.0000   +/-   1.0000        :N:A:D
  199  CDF_ZY_lum                             1.2921   +/-   0.0941        :N:M:D
  200  CDF_ZY_background_CC                   0.0350   +/-   0.9993        :N:A:D
  201  CDF_ZY_background_CF                  -0.1701   +/-   0.9156        :N:A:D
  202  CDF_ZY_background_FF                  -0.9307   +/-   0.7723        :N:A:D
  203  CDF_ZY_CMaterial                      -0.3472   +/-   0.9615        :N:M:D
  204  CDF_ZY_FMaterial                      -0.2203   +/-   0.9861        :N:M:D
  205  CDF_ZY_ZVtx                            0.3975   +/-   0.9263        :N:M:D
  206  CDF_ZY_Trkeff                         -0.1659   +/-   0.9510        :N:M:D
  207  CDF_ZY_NoTrk                           0.2400   +/-   0.9743        :N:M:D
  208  CDF_WA_chmisid                         0.0936   +/-   0.9296        :N:A:D
  209  CDF_WA_background                     -0.8608   +/-   0.9303        :N:A:D
  210  CDF_WA_energyscale                     0.4396   +/-   0.8851        :N:A:D
  211  CDF_WA_recoil                         -0.7710   +/-   0.5476        :N:A:D
  212  CDF_WA_pdf                            -0.3824   +/-   0.8797        :N:A:D
