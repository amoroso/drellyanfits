# drellyanfits

## CMS measurements TODO
* [ ] profiling ad NNLO QCD + NLO EW per yZ CMS 13TeV
* [ ] updated yZ and yW grids with new EW scheme/parameters
* [ ] nuovi NNLO QCD k-factors per CMS yW 13 TeV 
* [ ] check of profiling with grids with different EW scheme/parameters
* [ ] NNLO QCD k-factors per CMS W-helicity 13 TeV
* [ ] chi2/profiling ad NLO QCD per CMS AFB 7TeV
* [ ] grids per CMS AFB 8 TeV
* [ ] grids per CMS eta_lepton 13 TeV
* [ ] NNLO QCD per CMS eta_lepton 13 TeV
* [ ] Powheg NLO EW + s2eff variations per CMS AFB 7+8 TeV
* [ ] SANC NLO EW k-factors for CMS 13TeV eta_lepton ad helicity cross-sections (Renat)
* [ ] Update grids for CMS eta_lepton at 7 and 8 TeV
* [ ] Update NNLO QCD k-factors for CMS eta_lepton at 7 and 8 TeV
* [ ] grids for CMS Z2D (mass x rapidity) at 7 TeV
* [ ] NNLO QCD k-factors for CMS Z2D (mass x rapidity) at 7 TeV

## ATLAS measurements TODO
* [ ] DYTURBO NNLO QCD k-factors for W,Z 5 TeV, 2.76 TeV and W 8 TeV
* [ ] NLO EW k-factors for eta_lep measurements (Renat)

## LHCb measurements TODO
* [ ] DYTURBO k-factors for all measurements

## Tevatron measurements TODO
* [ ] grids for new CDF full Run2 eta_lepton
* [ ] NNLO QCD k-factors for CDF full Run2 eta_lepton
* [ ] Total cross-sections at 1.8, 1.96 TeV from Tevatron (grids + k-factors)

## Other measurements
* [ ] yZ and eta_lep at 510 GeV from STAR
* [ ] Total W and Z cross-sections from UA2 (?)

