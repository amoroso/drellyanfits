fewz_z_atlas_7_tev_cc_low_ymumu.txt:
ew k-factors for |y(mu+ mu-)| distribution for Z production at ATLAS 7 TeV (pT(mu+) > 20 GeV, pT(mu-) > 20 GeV, |eta(mu+)| < 2.5, |eta(mu-)| < 2.5, 46 GeV < m(mu+ mu-) < 66 GeV)

fewz_z_atlas_7_tev_cc_high_ymumu.txt:
ew k-factors for |y(mu+ mu-)| distribution for Z production at ATLAS 7 TeV (pT(mu+) > 20 GeV, pT(mu-) > 20 GeV, |eta(mu+)| < 2.5, |eta(mu-)| < 2.5, 116 GeV < m(mu+ mu-) < 150 GeV)

fewz_z_atlas_7_tev_cc_peak_ymumu.txt:
ew k-factors for |y(mu+ mu-)| distribution for Z production at ATLAS 7 TeV (pT(mu+) > 20 GeV, pT(mu-) > 20 GeV, |eta(mu+)| < 2.5, |eta(mu-)| < 2.5, 66 GeV < m(mu+ mu-) < 116 GeV)